//
// created by Ofir & Shawn on 29/08/2017.
//

#ifndef CHESS_SPMAINAUX_H
#define CHESS_SPMAINAUX_H

#include "core/SPGameState.h"
#include "core/SPGameActions.h"
#include "util/SPMessages.h"
#include "util/SPParser.h"
#include "core/SPMiniMax.h"

/**
 * runs a loop that reads user's commands to specify game settings.
 * @return
 * a SPGameState instance with the user's settings
 */
SPGameState* getUserSettings();

/**
 * handles a user's command to change game difficulty.
 * @param gameState - game state with previous settings
 * @param command - the user's command
 */
void handleDifficultyCmd(SPGameState* gameState, SPCommand *command);

/**
 * handles a user's command to change the game mode.
 * @param gameState - game state with previous settings
 * @param command - the user's command
 */
void handleGameModeCmd(SPGameState* gameState, SPCommand *command);

/**
 * handles a user's command to change his pieces' color
 * @param gameState - game state with previous settings
 * @param command - the user's command
 */
void handleUserColorCmd(const SPGameState *gameState, SPCommand *command);

/**
 * handles a user's command to load a game from file
 * @param gameState - game state with previous settings
 * @param command - the user's command
 */
void handleLoadCmd(SPGameState** gameState, SPCommand *command);

/**
 * handles a user's command to revert game settings to their default state.
 * @param gameState - game state with previous settings
 * @param command - the user's command
 */
void handleDefaultCmd(SPGameState *gameState, SPCommand *command);

/**
 * handles a user's command to print the current game's settings
 * @param gameState - game state instance
 * @param command - the user's command
 */
void handlePrintSettingsCmd(SPGameState *gameState, SPCommand *command);

/**
 * handles a user's command to quit the application.
 * @param gameState - game state instance
 * @param command - the user's command
 */
void handleQuitCmd(SPGameState *gameState, SPCommand *command);

/**
 * makes a move using the minimax algorithm. prints messages to the console according to the move result (check, checkmate, etc...)
 * @param gameState - game state instance
 * @return
 * SPMoveResult instance with the move's results (check, checkmate, etc...)
 */
SPMoveResult makePCMove(SPGameState *gameState);

/**
 * gets a game state, a user's command to make a move, and a pointer to a boolean.
 * the function tries to make the requested move, and prints messages according to its result (illegal move, check, checkmate, tie, etc...).
 * if the move was successful AND the game is not over then the function updates the boolean pointed by the given pointer to true.
 * @param gameState - game state instance
 * @param command - the user's move command
 * @param shouldPrintBoardAfterwards - pointer to a boolean, its value will be set to true if the move was successful AND the game is not over
 * @return
 * SPMoveResult instance with the move's results (check, checkmate, etc...)
 */
SPMoveResult makeUserMove(SPGameState* gameState, SPCommand command, bool* shouldPrintBoardAfterwards);

/**
 * handles a user's command to undo the last 2 moves.
 * if the history is empty, or the game mode is set to 2 players,
 * then the function will only print a message and won't change the game state.
 * @param gameState - game state instance
 * @param command - the user's command
 * @return
 * true on success to undo the last 2 moves, false otherwise.
 */
bool handleUndoMoveCommand(SPGameState *gameState, SPCommand cmd);

/**
 * handles a user's command to save the game to a file.
 * @param gameState - game state instance
 * @param command - the user's command
 */
void handleSaveCmd(SPGameState *gameState, SPCommand *command);

/**
 * the main loop of the console chess game. awaits user's commands (move, undo, save, etc...) and acts accordingly
 * @param gameState - @NotNULL game state instance
 */
void mainLoop(SPGameState *gameState);

/**
 * starts a console game by getting the user's settings and then starting the main game loop
 */
void startConsoleGame();

#endif //CHESS_SPMAINAUX_H
