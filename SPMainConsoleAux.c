//
// created by Ofir & Shawn on 29/08/2017.
//

#include "SPMainConsoleAux.h"


SPGameState *getUserSettings() {
    SPSettings *settings = createDefaultSettings();
    if (settings == NULL) {
        exit(1);
    }
    SPGameState *gameState = createGameState(settings);
    if (gameState == NULL) {
        free(settings);
        exit(1);
    }
    printf(MSG_SPECIFY_SETTING);

    while (true) {
        char *input = getUserInput();
        SPCommand command = parseLine(input);
        free(input);
        switch (command.cmd) {
            case SP_GAME_MODE_CMD:
                handleGameModeCmd(gameState, &command);
                break;

            case SP_DIFFICULTY_CMD:
                handleDifficultyCmd(gameState, &command);
                break;

            case SP_USER_COLOR_CMD:
                handleUserColorCmd(gameState, &command);
                break;

            case SP_LOAD_CMD:
                handleLoadCmd(&gameState, &command);
                break;

            case SP_DEFAULT_CMD:
                handleDefaultCmd(gameState, &command);
                break;

            case SP_PRINT_SETTINGS_CMD:
                handlePrintSettingsCmd(gameState, &command);
                break;

            case SP_QUIT_CMD:
                handleQuitCmd(gameState, &command);
                break;

            case SP_START_CMD:
                if (command.isValidArg) {
                    return gameState;
                } else {
                    printf(MSG_UNKNOWN_ARG);
                }
                break;

            default:
                printf(MSG_INVALID_COMMAND);
                break;
        }
    }
}

void handleDifficultyCmd(SPGameState *gameState, SPCommand *command) {
    if (gameState->settings->game_mode == 2) {
        printf(MSG_INVALID_COMMAND);
    } else if (!command->isValidArg) {
        printf(MSG_WRONG_DIFFICULTY);
    } else if (!setDifficulty(gameState->settings, (*command).x)) {
        printf((*command).x == 5 ? MSG_EXPERT_NOT_SUPPORTED : MSG_WRONG_DIFFICULTY);
    }
}

void handleGameModeCmd(SPGameState *gameState, SPCommand *command) {
    if (!command->isValidArg) {
        printf(MSG_WRONG_GAME_MODE);
    } else if (setGameMode(gameState->settings, (*command).x)) {
        printf((*command).x == 1 ? MSG_GAME_MODE_SET_1 : MSG_GAME_MODE_SET_2);
    } else {
        printf(MSG_WRONG_GAME_MODE);
    }
}

void handleUserColorCmd(const SPGameState *gameState, SPCommand *command) {
    if (!(*command).isValidArg || gameState->settings->game_mode == 2 ||
        !setUserColor(gameState->settings, (*command).x)) {
        printf(MSG_INVALID_COMMAND);
    }
}

void handleLoadCmd(SPGameState **gameState, SPCommand *command) {
    if (!command->isValidArg) {
        printf(MSG_NO_PATH);
        return;
    }

    SPGameState *loadedGame = createGameStateFromFile(command->path);
    free(command->path);
    if (loadedGame == NULL) {
        printf(MSG_FILE_ERR);
        return;
    }
    destroyGameState(*gameState);
    *gameState = loadedGame;
}

void handleDefaultCmd(SPGameState *gameState, SPCommand *command) {
    if ((*command).isValidArg) {
        destroySettings(gameState->settings);
        gameState->settings = createDefaultSettings();
    } else {
        printf(MSG_UNKNOWN_ARG);
    }
}

void handlePrintSettingsCmd(SPGameState *gameState, SPCommand *command) {
    if ((*command).isValidArg) {
        printSettings(gameState->settings);
    } else {
        printf(MSG_UNKNOWN_ARG);
    }
}

void handleQuitCmd(SPGameState *gameState, SPCommand *command) {
    if ((*command).isValidArg) {
        printf(MSG_EXITING);
        destroyGameState(gameState);
        exit(0);
    } else {
        printf(MSG_UNKNOWN_ARG);
    }
}

SPMoveResult makePCMove(SPGameState *gameState) {
    SPMove move = suggestMove(gameState); // find best move using minimax algorithm

    // print the move details
    char *pieceName = pieceCharToString(gameState->board[move.x][move.y]);
    printf(MSG_PC_MOVE, pieceName, move.x + 1, columnIntToChar(move.y + 1), move.z + 1, columnIntToChar(move.w + 1));

    // makes the move and prints messages accordingly
    SPMoveResult result = makeMove(gameState, move.x, move.y, move.z, move.w);
    if (result.tie) {
        printf(MSG_TIE_PC);
    } else if (result.checkmate) {
        printf("%s", result.msg);
    } else if (result.check) {
        printf(MSG_CHECK_PC);
    }
    return result;
}

SPMoveResult makeUserMove(SPGameState *gameState, SPCommand command, bool *shouldPrintBoardAfterwards) {
    SPMoveResult moveResult = makeMove(gameState, command.x - 1, columnCharToInt(command.y) - 1, command.z - 1,
                                       columnCharToInt(command.w) - 1);
    if (!moveResult.success || moveResult.checkmate || moveResult.tie) {
        printf("%s", moveResult.msg);
    } else if (moveResult.check) {
        printf("%s", moveResult.msg);
        (*shouldPrintBoardAfterwards) = true;
    } else if (moveResult.success) {
        (*shouldPrintBoardAfterwards) = true;
    }
    return moveResult;
}

bool handleUndoMoveCommand(SPGameState *gameState, SPCommand cmd) {
    if (!cmd.isValidArg) {
        printf(MSG_UNKNOWN_ARG);
    } else if (gameState->settings->game_mode == 2) {
        printf(MSG_UNDO_UNAVAILABLE);
    } else if (gameState->history->actualSize < 2) {
        printf(MSG_EMPTY_HISTORY);
    } else {
        undoMove(gameState, true);
        undoMove(gameState, true);
        return true;
    }
    return false;
}

void handleSaveCmd(SPGameState *gameState, SPCommand *command) {
    if (!(*command).isValidArg) {
        printf(MSG_NO_PATH);
    } else if (saveGameStateToFile(gameState, (*command).path)) {
        free((*command).path);
    } else {
        free((*command).path);
        printf(MSG_SAVE_ERR);
    }
}

void mainLoop(SPGameState *gameState) {
    SPMoveResult moveResult = {.success = false, .check = false, .checkmate = false, .tie = false, .msg = NULL};
    bool shouldPrintBoard = true;

    while (!moveResult.checkmate && !moveResult.tie) {
        if (isPCTurn(gameState)) {
            moveResult = makePCMove(gameState);
            continue;
        }

        if (shouldPrintBoard) {
            printGameBoard(gameState);
            shouldPrintBoard = false;
        }
        printf(gameState->currentPlayer == PLAYER_WHITE ? MSG_WHITE_ENTER_MOVE : MSG_BLACK_ENTER_MOVE);
        char *input = getUserInput();
        SPCommand command = parseLine(input);
        free(input);

        switch (command.cmd) {
            case SP_MOVE_CMD:
                if (!command.isValidMoveCmdStructure) {
                    printf(MSG_INVALID_COMMAND);
                } else if (!command.isValidArg) {
                    printf(MSG_INVALID_POSITION);
                } else {
                    moveResult = makeUserMove(gameState, command, &shouldPrintBoard);
                }
                break;

            case SP_UNDO_MOVE_CMD:
                shouldPrintBoard = handleUndoMoveCommand(gameState, command);
                break;

            case SP_SAVE_CMD:
                handleSaveCmd(gameState, &command);
                break;

            case SP_RESTART_CMD:
                if (!command.isValidArg) {
                    printf(MSG_UNKNOWN_ARG);
                } else {
                    destroyGameState(gameState);
                    printf(MSG_RESTARTING);
                    startConsoleGame();
                    return;
                }
                break;

            case SP_QUIT_CMD:
                handleQuitCmd(gameState, &command);
                break;

            default:
                printf(MSG_INVALID_COMMAND);
                break;
        }
    }
    destroyGameState(gameState);
}

void startConsoleGame() {
    SPGameState *gameState = getUserSettings();
    mainLoop(gameState);
}
