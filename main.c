//
// created by Ofir & Shawn on 22/08/2017.
//

#include "SPMainConsoleAux.h"
#include "SPMainGUIAux.h"

int main(int argc, char **argv) {
    if (argc == 1 || (argc == 2 && strcmp(argv[1], "-c") == 0)) {
        startConsoleGame();
    } else if (argc == 2 && strcmp(argv[1], "-g") == 0) {
        return startGUI();
    } else {
        printf("Error: invalid argument");
        return 1;
    }

    return 0;
}
