//
// created by Ofir & Shawn on 12/09/2017.
//

#include "SPMainGUIAux.h"

void guiMakePCMove(SPGuiManager *guiManager) {
    if (guiManager->gameBoardWin == NULL || guiManager->gameBoardWin->gameState == NULL) {
        return;
    }

    SPGameState *gameState = guiManager->gameBoardWin->gameState;
    if (isPCTurn(gameState)) {
        gameWindowDraw(guiManager->gameBoardWin); // first, let the user watch his own move on the board
        SPMove move = suggestMove(gameState);
        SPMoveResult result = makeMove(gameState, move.x, move.y, move.z, move.w);
        if (result.checkmate || result.tie || result.check) {
            gameWindowDraw(guiManager->gameBoardWin);
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Message", result.msg, NULL);
        }
        if (result.checkmate || result.tie) {
            guiManager->gameBoardWin->isGameOver = true;
        }
    }

}

int startGUI() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) { //SDL2 INIT
        printf("ERROR: unable to init SDL: %s\n", SDL_GetError());
        return 1;
    }
    SPGuiManager *guiManager = guiManagerCreate();
    if (guiManager == NULL) {
        SDL_Quit();
        return 1;
    }

    SDL_Event event;
    SP_EVENT eventHandlingRes;
    bool firstIteration = true;
    while (1) {
        SDL_WaitEvent(&event);
        if ((eventHandlingRes = guiManagerHandleEvent(guiManager, &event)) == EVENT_QUIT) {
            break;
        }
        guiMakePCMove(guiManager); // now make a PC game move if needed
        if (firstIteration || eventHandlingRes != EVENT_NONE) {
            guiManagerDraw(guiManager);
            firstIteration = false;
        }
    }
    guiManagerDestroy(guiManager);
    SDL_Quit();
    return 0;
}
