//
// created by Ofir & Shawn on 23/08/2017.
//

#include "SPXMLFile.h"

SPXMLFile* createXMLFile() {
    SPXMLFile* xml = malloc(sizeof(SPXMLFile));
    if (!xml) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    xml->currentTurn = '\0';
    xml->gameMode ='\0';
    xml->difficulty = '\0';
    xml->userColor = '\0';
    for (int i = 0; i < 8; i++) {
        xml->rows[i] = NULL;
    }
    return xml;
}

char* fileToString(char* path) {
    FILE* file = fopen(path, "r");
    if (!file) {
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    int length = ftell(file);
    fseek(file, 0, SEEK_SET);

    char* content = malloc(length+1);
    if (!content) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    fread(content, 1, length, file);
    fclose(file);
    content[length] = '\0';
    return content;
}

SPXMLFile* loadXMLFromFile(char* path) {
    char* content = fileToString(path);
    if (!content) {
        return NULL;
    }
    SPXMLFile* xml = createXMLFile();
    if (!xml) {
        free(content);
        return NULL;
    }

    xml->currentTurn = extractCharTag(content, CURRENT_TURN_TAG);
    xml->gameMode = extractCharTag(content, GAME_MODE_TAG);
    xml->difficulty = extractCharTag(content, DIFFICULTY_TAG);
    xml->userColor = extractCharTag(content, USER_CLR_TAG);
    xml->rows[0] = extractStringTag(content, ROW1, 8);
    xml->rows[1] = extractStringTag(content, ROW2, 8);
    xml->rows[2] = extractStringTag(content, ROW3, 8);
    xml->rows[3] = extractStringTag(content, ROW4, 8);
    xml->rows[4] = extractStringTag(content, ROW5, 8);
    xml->rows[5] = extractStringTag(content, ROW6, 8);
    xml->rows[6] = extractStringTag(content, ROW7, 8);
    xml->rows[7] = extractStringTag(content, ROW8, 8);
    free(content);
    return xml;
}

char extractCharTag(char* content, char* tag) {
    char* value = strstr(content, tag);
    if (!value) {
        return '\0';
    }
    return value[strlen(tag)];
}

char* extractStringTag(char* content, char* tag, int length) {
    char* value = strstr(content, tag);
    if (!value) {
        return NULL;
    }
    char* result = malloc(length + 1);
    if (!result) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    memcpy(result, value + strlen(tag), length);
    result[length] = '\0';
    return result;
}

bool printXMLToFile(SPXMLFile *xml, char *path) {
    if (!xml) {
        return false;
    }
    FILE* file = fopen(path, "w");
    if (!file) {
        return false;
    }

    int result = fprintf(file, FILE_FORMAT_PART1, xml->currentTurn, xml->gameMode);
    if (result > 0 && xml->difficulty != '\0') {
        result = fprintf(file, FILE_FORMAT_PART2, xml->difficulty);
    }
    if (result > 0 && xml->userColor != '\0') {
        result = fprintf(file, FILE_FORMAT_PART3, xml->userColor);
    }
    if (result > 0) {
       result = fprintf(file, FILE_FORMAT_PART4, xml->rows[7], xml->rows[6], xml->rows[5], xml->rows[4], xml->rows[3],
                xml->rows[2], xml->rows[1], xml->rows[0]);
    }
    fclose(file);
    return result > 0;
}

void destroyXMLFile(SPXMLFile* xml) {
    if (xml == NULL) {
        return;
    }
    for (int i = 0; i < 8; i++) {
        if (xml->rows[i] != NULL) {
            free(xml->rows[i]);
        }
    }
    free(xml);
}