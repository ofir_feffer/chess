//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPMINIMAX_H
#define CHESS_SPMINIMAX_H

#include "SPGameActions.h"
#include <limits.h>
#include "../util/SPUtil.h"

#define UNINITIALIZED_INT_VALUE INT_MAX-1
#define MSG_NON_TERMINAL_NODE_HAS_NO_CHILDREN "Error: non-terminal minimax node has no children"

typedef struct miniMaxNode {
    SPGameState* gameState;
    SPMove* move; // the move that created that node
} SPMiniMaxNode;

/**
 * creates a new MiniMax Node instance with a copy of the given gameState.
 * move field will be set as NULL
 * @param gameState - @NotNULL game state instance
 * @return NULL if an allocation error occurs, SPMiniMaxNode instance otherwise
 */
SPMiniMaxNode* createNode(SPGameState* gameState);

/**
 * creates a new MiniMax Node instance. move field will be set as the given move.
 * the given move will be made on a copy of the given game state,
 * and the resulted game state will be the node's gameState field
 * @param gameState - @NotNULL game state instance
 * @param move - @NotNULL chess move
 * @pre the chess move is legal in the given game state
 * @return NULL if an allocation error occurs, SPMiniMaxNode instance otherwise
 */
SPMiniMaxNode* createNodeFromMove(SPGameState *gameState, SPMove* move);

/**
 * creates a list of all possible children of a given MiniMax node.
 * @param node - @NotNULL a MiniMax node
 * @return NULL if an allocation error occurs, SPLinkedList of MiniMax nodes otherwise
 */
SPLinkedList* createChildrenList(SPMiniMaxNode* node);

/**
 * calculates the score of a given game state, relative to a given player.
 * if the game state is terminal (game is over by tie or checkmate),
 * then the function will set the boolean pointed by the 'terminalNode' pointer to true, otherwise will set it to false
 * @param gameState - @NotNULL game state instance
 * @param player - 0 for black, 1 for white
 * @param terminalNode - pointer to a boolean. the function will set its value to true if the game state is terminal and to false otherwise
 * @return score of the given game state.
 */
int calculateGameStateScore(SPGameState* gameState, int player, bool* terminalNode);

/**
 * finds the optimal move for the current player using the minimax algorithm with depth equal to the game's difficulty
 * @param gameState - @NotNULL game state instance
 * @return SPMove instance with the optimal move
 */
SPMove suggestMove(SPGameState* gameState);

/**
 * finds the best move using the minimax algorithm with alpha-beta pruning.
 * @param node - a MiniMax node
 * @param depth - current depth of the given node (distance from initiating node)
 * @param maxDepth - maximum depth
 * @param maximizingPlayer - boolean value. should be true if node is in a even depth, and false otherwise
 * @param initiatingPlayer - the player for which we are searching for the best move
 * @param result - a pointer to a SPMove variable, in which the best move will be put.
 * @return score of the given minimax node
 */
int alphaBeta(SPMiniMaxNode* node, int depth, int maxDepth, int alpha, int beta, bool maximizingPlayer, int initiatingPlayer, SPMove* result);

/**
 * util function of the alphaBeta function. finds the node with the minimal score among all children of a given node.
 * @param node - a MiniMax node
 * @param depth - current depth of the given node (distance from initiating node)
 * @param maxDepth - maximum depth
 * @param initiatingPlayer - the player for which we are searching for the best move
 * @return the minimal score of the given node's children
 */
int findMinNode(SPMiniMaxNode *node, int depth, int maxDepth, int alpha, int beta, int initiatingPlayer);

/**
 * util function of the alphaBeta function. finds the node with the maximal score among all children of a given node.
 * @param node - a MiniMax node
 * @param depth - current depth of the given node (distance from initiating node)
 * @param maxDepth - maximum depth
 * @param initiatingPlayer - the player for which we are searching for the best move
 * @param result - a pointer to a SPMove variable, in which the best move will be put.
 * @return the maximal score of the given node's children
 */
int findMaxNode(SPMiniMaxNode *node, int depth, int maxDepth, int alpha, int beta, int initiatingPlayer, SPMove *result);

/**
 * frees all memory associated with a given MiniMax node.
 * if the source node is NULL then the function does nothing
 * @param node - a SPMiniMaxNode instance
 */
void destroyMiniMaxNode(SPMiniMaxNode* node);

/**
 * frees all memory associated with a list of MiniMax nodes.
 * @param listNode - a SPNode instance, which is NULL or contains data of a SPMiniMaxNode
 * @pre all nodes in the list contain data of MiniMax nodes
 */
void destroyRemainingChildren(SPNode* listNode);

#endif //CHESS_SPMINIMAX_H
