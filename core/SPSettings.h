//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPSETTINGS_H
#define CHESS_SPSETTINGS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../util/SPMessages.h"

#define SETTINGS "SETTINGS:\n"
#define GAME_MODE_MSG "GAME_MODE: %d\n"
#define DIFFICULTY_MSG "DIFFICULTY_LVL: %d\n"
#define USER_CLR_MSG "USER_CLR: %s\n"
#define BLACK_MSG "BLACK"
#define WHITE_MSG "WHITE"

typedef struct settings {
    int game_mode;
    int difficulty;
    int user_color;
} SPSettings;

/**
 * creates a set of default settings.
 *@return a SPSettings instance with default values, or NULL if an allocation error occurs
*/
SPSettings* createDefaultSettings();

/**
 * creates settings with the same attributes as the one received in input
 *@param source - source SPSettings instance
 *@return NULL if source SPSettings is NULL or if an allocation error occurs, copy of the SPSettings otherwise
*/
SPSettings* copySettings(SPSettings* source);

/**
 * receives a SPSettings instance and an integer representing the game_mode.
 * sets the setting's game mode to the integer we received in the input.
 * @param settings - a SPSettings instance
 * @param mode - integer representing the desired game mode
 * @return true if success, false if one of the input parameters were illegal
*/
bool setGameMode(SPSettings* settings, int mode);

/**
 * receives a SPSettings instance and an integer representing the game difficulty.
 * sets the setting's difficulty to the integer we received in the input.
 * @param settings - a SPSettings instance
 * @param difficulty - integer representing the desired game difficulty
 * @return true if success, false if one of the input parameters were illegal
*/
bool setDifficulty(SPSettings* settings, int difficulty);

/**
 * receives a SPSettings instance and an integer representing the user color.
 * sets the setting's user color to the integer we received in the input.
 * @param settings - a SPSettings instance
 * @param mode - integer representing the desired user color
 * @return true if success, false if one of the input parameters were illegal
*/
bool setUserColor(SPSettings *settings, int color);

/**
 * prints to console the settings represented by a SPSettings instance.
 * if input SPSettings is NULL then the function does nothing
 * @param settings - a SPSettings instance
 */
void printSettings(SPSettings* settings);

/**
 * frees all memory resources associated with a SPSettings instance.
 * if input SPSettings is NULL then the function does nothing
 * @param settings - a SPSettings instance
 */
void destroySettings(SPSettings* settings);

#endif //CHESS_SPSETTINGS_H
