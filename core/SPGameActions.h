//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPGAMEACTIONS_H
#define CHESS_SPGAMEACTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "SPGameState.h"
#include "../util/SPMessages.h"
#include "../util/SPLinkedList.h"
#include "../util/SPArrayList.h"

typedef struct actionResult {
    bool success;
    bool check;
    bool checkmate;
    bool tie;
    const char* msg;
} SPMoveResult;

/**
 * returns the coordinates of a player's king piece on the gameboard.
 * @param gameState - @NotNULL gameState instance
 * @param color - color of player (0 for black, 1 for white)
 * @return coordinates of king's piece location
*/
SPTuple getKingLocationOf(SPGameState* gameState, int color);

/**
 * the function checks whether the piece in input is a black or white king,
 * and update's the king location variable in the gameState instance
 * @param gameState - @NotNULL gameState instance
 * @param piece - a chess piece
 * @param x, y - coordinates of the chess piece on the game board
*/
void updateKingLocationIfNecessary(SPGameState *gameState, char piece, int x, int y);

/**
 * checks which piece is in (x,y), the coordinates in input, and returns a linked-list of legal moves for that piece.
 * @param gameState - gameState instance
 * @param x, y - coordinates of a chess piece on the game board
 * @return linked list of legal moves for the piece in (x,y)
*/
SPLinkedList* getMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a pawn on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a pawn piece on the game board
 * @pre (x,y) are valid indexes of a pawn piece
 * @return linked-list of all legal moves for the specified pawn
*/
SPLinkedList* getPawnMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a bishop on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a bishop piece on the game board
 * @pre (x,y) are valid indexes of a bishop piece
 * @return linked-list of all legal moves for the specified bishop
*/
SPLinkedList* getBishopMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a rook on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a rook piece on the game board
 * @pre (x,y) are valid indexes of a rook piece
 * @return linked-list of all legal moves for the specified rook
*/
SPLinkedList* getRookMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a knight on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a knight piece on the game board
 * @pre (x,y) are valid indexes of a knight piece
 * @return linked-list of all legal moves for the specified knight
*/
SPLinkedList* getKnightMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a queen on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a queen piece on the game board
 * @pre (x,y) are valid indexes of a queen piece
 * @return linked-list of all legal moves for the specified queen
*/
SPLinkedList* getQueenMoves(SPGameState *gameState, int x, int y);

/**
 * creates a list of all possible moves of a king on the game board.
 * @param gameState - @NotNULL gameState instance
 * @param x, y - coordinates of a king piece on the game board
 * @pre (x,y) are valid indexes of a king piece
 * @return linked-list of all legal moves for the specified king
*/
SPLinkedList* getKingMoves(SPGameState *gameState, int x, int y);

/**
 * checks if a player's king is threatened by the opponent
 * @param gameState - @NotNULL gameState instance
 * @param player - player's color (0 for black, 1 for white)
 * @return true if the player's king is threatened by the opponent, false otherwise
 */
bool isInCheck(SPGameState* gameState, int player);

/**
 * checks if a chess game is over.
 * @param gameState - @NotNULL gameState instance
 * @return true if the current player doesn't have any moves to make (therefore game is over), false otherwise
 */
bool isGameOver(SPGameState* gameState);

/**
 * checks if the move of the piece in (x,y) to (z,w) is legal according to the piece's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source piece coordinates
 * @param z,w - target cell
 * @param player - the executing player
 * @return true if legal piece movement, false otherwise
 */
bool isLegalPieceMovement(SPGameState *gameState, int x, int y, int z, int w, int player);

/**
 * checks if the move of the pawn in (x,y) to (z,w) is legal according to the pawn's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source pawn coordinates
 * @param z,w - target cell
 * @param player - the executing player
 * @pre the pawn belongs to the executing player
 * @return true if legal pawn movement, false otherwise
 */
bool isLegalPawnMovement(SPGameState *gameState, int x, int y, int z, int w, int player);

/**
 * checks if the move of the bishop in (x,y) to (z,w) is legal according to the bishop's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source bishop coordinates
 * @param z,w - target cell
 * @return true if legal bishop movement, false otherwise
 */
bool isLegalBishopMovement(SPGameState *gameState, int x, int y, int z, int w);

/**
 * checks if the move of the rook in (x,y) to (z,w) is legal according to the rook's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source rook coordinates
 * @param z,w - target cell
 * @return true if legal rook movement, false otherwise
 */
bool isLegalRookMovement(SPGameState *gameState, int x, int y, int z, int w);

/**
 * checks if the move of the knight in (x,y) to (z,w) is legal according to the knight's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source knight coordinates
 * @param z,w - target cell
 * @return true if legal knight movement, false otherwise
 */
bool isLegalKnightMovement(int x, int y, int z, int w);

/**
 * checks if the move of the queen in (x,y) to (z,w) is legal according to the queen's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source queen coordinates
 * @param z,w - target cell
 * @return true if legal queen movement, false otherwise
 */
bool isLegalQueenMovement(SPGameState *gameState, int x, int y, int z, int w);

/**
 * checks if the move of the king in (x,y) to (z,w) is legal according to the king's movement policy.
 * the function does NOT check if the move exposes the player's king
 * @param x,y - source king coordinates
 * @param z,w - target cell
 * @return true if legal king movement, false otherwise
 */
bool isLegalKingMovement(int x, int y, int z, int w);

/**
 * The function returns true if the move (x,y) to (z,w) is a legal move, and false otherwise
 * @param gameState - @NotNULL gameState instance
 * @param x,y - source piece coordinates
 * @param z,w - target cell
 * @param player - the executing player
 * @return true if legal move, false if illegal move
 */
bool isLegalMove(SPGameState *gameState, int x, int y, int z, int w, int player);

/**
 * checks if the move of the piece in (x,y) to (z,w) is legal, and if so updates the board,
 * and updates the game's history. the function then checks whether there is a check, checkmate or tie in the game,
 * and returns the result.
 * @param gameState - gameState instance
 * @param x,y - source piece coordinates
 * @param z,w - target cell
 * @return SPMoveResult instance indicating the move's success/failure status and if it resulted in a check, checkmate or tie
*/
SPMoveResult makeMove(SPGameState* gameState, int x, int y, int z, int w);

/**
 * The function returns the gameState to its previous state, and updates the gameState's history.
 * @pre the game mode is set to 1 player AND history is not empty
 * @param gameState - @NotNULL gameState instance
 * @param verbose - boolean indicating whether or not the function should print to console the move that was undone.
*/
void undoMove(SPGameState *gameState, bool verbose);

#endif //CHESS_SPGAMEACTIONS_H
