//
// created by Ofir & Shawn on 22/08/2017.
//

#include "SPGameActions.h"

SPTuple getKingLocationOf(SPGameState *gameState, int color) {
    if (color == PLAYER_WHITE) {
        return gameState->whiteKingLocation;
    } else {
        return gameState->blackKingLocation;
    }
}

void updateKingLocationIfNecessary(SPGameState *gameState, char piece, int x, int y) {
    if (piece == WHITE_KING) {
        gameState->whiteKingLocation.x = x;
        gameState->whiteKingLocation.y = y;
    } else if (piece == BLACK_KING) {
        gameState->blackKingLocation.x = x;
        gameState->blackKingLocation.y = y;
    }
}

SPLinkedList *getMoves(SPGameState *gameState, int x, int y) {
    char piece;
    if (gameState == NULL || (piece = gameState->board[x][y]) == EMPTY_CELL) {
        return NULL;
    }

    switch (piece) {
        case WHITE_PAWN:
        case BLACK_PAWN:
            return getPawnMoves(gameState, x, y);
        case WHITE_BISHOP:
        case BLACK_BISHOP:
            return getBishopMoves(gameState, x, y);
        case WHITE_ROOK:
        case BLACK_ROOK:
            return getRookMoves(gameState, x, y);
        case WHITE_KNIGHT:
        case BLACK_KNIGHT:
            return getKnightMoves(gameState, x, y);
        case WHITE_QUEEN:
        case BLACK_QUEEN:
            return getQueenMoves(gameState, x, y);
        case WHITE_KING:
        case BLACK_KING:
            return getKingMoves(gameState, x, y);
        default:
            return NULL;
    }
}

SPLinkedList *getPawnMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *result = createLinkedList();
    if (result == NULL) {
        return NULL;
    }

    int player = colorOfPiece(gameState->board[x][y]);
    int mf = movementFactorOfPawn(player);
    //there are 4 possible moves. check each and add
    if (isIndexInBounds(x + mf, y) && isLegalMove(gameState, x, y, x + mf, y, player)) {
        SPTuple* move = createTuple(x + mf, y);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 2 * mf, y) && isLegalMove(gameState, x, y, x + 2 * mf, y, player)) {
        SPTuple* move = createTuple(x + 2 * mf, y);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + mf, y + 1) && isLegalMove(gameState, x, y, x + mf, y + 1, player)) {
        SPTuple* move = createTuple(x + mf, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + mf, y - 1) && isLegalMove(gameState, x, y, x + mf, y - 1, player)) {
        SPTuple* move = createTuple(x + mf, y - 1);
        addItemToLinkedList(result, move);
    }

    return result;
}

SPLinkedList *getBishopMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *result = createLinkedList();
    if (result == NULL) {
        return NULL;
    }

    int player = colorOfPiece(gameState->board[x][y]);
    bool flag1 = isIndexInBounds(x + 1, y + 1), flag2 = isIndexInBounds(x - 1, y - 1),
            flag3 = isIndexInBounds(x + 1, y - 1), flag4 = isIndexInBounds(x - 1, y + 1);
    for (int i = 1; i < 8 && (flag1 || flag2 || flag3 || flag4); i++) {
        if (flag1) { //direction 1
            if (isLegalMove(gameState, x, y, x + i, y + i, player)) {
                SPTuple* move = createTuple(x + i, y + i);
                addItemToLinkedList(result, move);
            }
            flag1 = gameState->board[x + i][y + i] == EMPTY_CELL && isIndexInBounds(x + (i + 1), y + (i + 1));
        }
        if (flag2) { //direction 2
            if (isLegalMove(gameState, x, y, x - i, y - i, player)) {
                SPTuple* move = createTuple(x - i, y - i);
                addItemToLinkedList(result, move);
            }
            flag2 = gameState->board[x - i][y - i] == EMPTY_CELL && isIndexInBounds(x - (i + 1), y - (i + 1));
        }
        if (flag3) { //direction 3
            if (isLegalMove(gameState, x, y, x + i, y - i, player)) {
                SPTuple* move = createTuple(x + i, y - i);
                addItemToLinkedList(result, move);
            }
            flag3 = gameState->board[x + i][y - i] == EMPTY_CELL && isIndexInBounds(x + (i + 1), y - (i + 1));
        }
        if (flag4) { //direction 4
            if (isLegalMove(gameState, x, y, x - i, y + i, player)) {
                SPTuple* move = createTuple(x - i, y + i);
                addItemToLinkedList(result, move);
            }
            flag4 = gameState->board[x - i][y + i] == EMPTY_CELL && isIndexInBounds(x - (i + 1), y + (i + 1));
        }
    }

    return result;
}

SPLinkedList *getRookMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *result = createLinkedList();
    if (result == NULL) {
        return NULL;
    }

    int player = colorOfPiece(gameState->board[x][y]);
    bool flag1 = isIndexInBounds(x + 1, y), flag2 = isIndexInBounds(x - 1, y),
            flag3 = isIndexInBounds(x, y - 1), flag4 = isIndexInBounds(x, y + 1);
    for (int i = 1; i < 8 && (flag1 || flag2 || flag3 || flag4); i++) {
        if (flag1) { //direction 1
            if (isLegalMove(gameState, x, y, x + i, y, player)) {
                SPTuple* move = createTuple(x + i, y);
                addItemToLinkedList(result, move);
            }
            flag1 = gameState->board[x + i][y] == EMPTY_CELL && isIndexInBounds(x + (i + 1), y);
        }
        if (flag2) { //direction 2
            if (isLegalMove(gameState, x, y, x - i, y, player)) {
                SPTuple* move = createTuple(x - i, y);
                addItemToLinkedList(result, move);
            }
            flag2 = gameState->board[x - i][y] == EMPTY_CELL && isIndexInBounds(x - (i + 1), y);
        }
        if (flag3) { //direction 3
            if (isLegalMove(gameState, x, y, x, y - i, player)) {
                SPTuple* move = createTuple(x, y - i);
                addItemToLinkedList(result, move);
            }
            flag3 = gameState->board[x][y - i] == EMPTY_CELL && isIndexInBounds(x, y - (i + 1));
        }
        if (flag4) { //direction 4
            if (isLegalMove(gameState, x, y, x, y + i, player)) {
                SPTuple* move = createTuple(x, y + i);
                addItemToLinkedList(result, move);
            }
            flag4 = gameState->board[x][y + i] == EMPTY_CELL && isIndexInBounds(x, y + (i + 1));
        }
    }

    return result;
}

SPLinkedList *getKnightMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *result = createLinkedList();
    if (result == NULL) {
        return NULL;
    }

    int player = colorOfPiece(gameState->board[x][y]);
    //there are 8 possible moves. check each and add
    if (isIndexInBounds(x + 1, y + 2) && isLegalMove(gameState, x, y, x + 1, y + 2, player)) {
        SPTuple* move = createTuple(x + 1, y + 2);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 1, y - 2) && isLegalMove(gameState, x, y, x + 1, y - 2, player)) {
        SPTuple* move = createTuple(x + 1, y - 2);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 1, y + 2) && isLegalMove(gameState, x, y, x - 1, y + 2, player)) {
        SPTuple* move = createTuple(x - 1, y + 2);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 1, y - 2) && isLegalMove(gameState, x, y, x - 1, y - 2, player)) {
        SPTuple* move = createTuple(x - 1, y - 2);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 2, y + 1) && isLegalMove(gameState, x, y, x + 2, y + 1, player)) {
        SPTuple* move = createTuple(x + 2, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 2, y - 1) && isLegalMove(gameState, x, y, x + 2, y - 1, player)) {
        SPTuple* move = createTuple(x + 2, y - 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 2, y + 1) && isLegalMove(gameState, x, y, x - 2, y + 1, player)) {
        SPTuple* move = createTuple(x - 2, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 2, y - 1) && isLegalMove(gameState, x, y, x - 2, y - 1, player)) {
        SPTuple* move = createTuple(x - 2, y - 1);
        addItemToLinkedList(result, move);
    }

    return result;
}

SPLinkedList *getQueenMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *list1 = getBishopMoves(gameState, x, y);
    SPLinkedList *list2 = getRookMoves(gameState, x, y);
    SPLinkedList *result = mergeLists(list1, list2);
    return result;
}

SPLinkedList *getKingMoves(SPGameState *gameState, int x, int y) {
    SPLinkedList *result = createLinkedList();
    if (result == NULL) {
        return NULL;
    }

    int player = colorOfPiece(gameState->board[x][y]);
    //there are 8 possible moves. check each and add
    if (isIndexInBounds(x + 1, y) && isLegalMove(gameState, x, y, x + 1, y, player)) {
        SPTuple* move = createTuple(x + 1, y);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 1, y) && isLegalMove(gameState, x, y, x - 1, y, player)) {
        SPTuple* move = createTuple(x - 1, y);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x, y + 1) && isLegalMove(gameState, x, y, x, y + 1, player)) {
        SPTuple* move = createTuple(x, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x, y - 1) && isLegalMove(gameState, x, y, x, y - 1, player)) {
        SPTuple* move = createTuple(x, y - 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 1, y + 1) && isLegalMove(gameState, x, y, x + 1, y + 1, player)) {
        SPTuple* move = createTuple(x + 1, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x + 1, y - 1) && isLegalMove(gameState, x, y, x + 1, y - 1, player)) {
        SPTuple* move = createTuple(x + 1, y - 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 1, y + 1) && isLegalMove(gameState, x, y, x - 1, y + 1, player)) {
        SPTuple* move = createTuple(x - 1, y + 1);
        addItemToLinkedList(result, move);
    }
    if (isIndexInBounds(x - 1, y - 1) && isLegalMove(gameState, x, y, x - 1, y - 1, player)) {
        SPTuple* move = createTuple(x - 1, y - 1);
        addItemToLinkedList(result, move);
    }

    return result;
}

bool isInCheck(SPGameState *gameState, int player) {
    SPTuple kingLocation = getKingLocationOf(gameState, player);
    int opponent = oppositeColorOf(player);
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (isOpponentPiece(player, gameState->board[i][j]) &&
                isLegalPieceMovement(gameState, i, j, kingLocation.x, kingLocation.y, opponent)) {
                return true;
            }
        }
    }

    return false;
}

bool isGameOver(SPGameState *gameState) {
    int player = gameState->currentPlayer;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (isPlayerPiece(player, gameState->board[i][j])) {
                SPLinkedList *moves = getMoves(gameState, i, j);
                int size = (moves != NULL) ? moves->size : 0;
                destroyLinkedListOfTuples(moves);
                if (size > 0) {
                    return false;
                }
            }
        }
    }
    return true;
}

bool isLegalPieceMovement(SPGameState *gameState, int x, int y, int z, int w, int player) {
    char source = gameState->board[x][y];
    char target = gameState->board[z][w];
    if (isPlayerPiece(player, target)) {
        return false; // target square is occupied by current player's piece
    }

    if (source == BLACK_PAWN || source == WHITE_PAWN)
        return isLegalPawnMovement(gameState, x, y, z, w, player);
    else if (source == BLACK_KNIGHT || source == WHITE_KNIGHT)
        return isLegalKnightMovement(x, y, z, w);
    else if (source == BLACK_BISHOP || source == WHITE_BISHOP)
        return isLegalBishopMovement(gameState, x, y, z, w);
    else if (source == BLACK_ROOK || source == WHITE_ROOK)
        return isLegalRookMovement(gameState, x, y, z, w);
    else if (source == BLACK_QUEEN || source == WHITE_QUEEN)
        return isLegalQueenMovement(gameState, x, y, z, w);
    else if (source == BLACK_KING || source == WHITE_KING)
        return isLegalKingMovement(x, y, z, w);
    else
        return false;
}

bool isLegalPawnMovement(SPGameState *gameState, int x, int y, int z, int w, int player) {
    char target = gameState->board[z][w];
    int mf = movementFactorOfPawn(player); // 1 for white, -1 for black

    if (y == w && z == (x + mf)) { // movement of 1 square forward in same column
        if (target == EMPTY_CELL)
            return true;
        else
            return false;
    } else if (y == w && x == pawnStartingRow(player) &&
               z == (x + 2 * mf)) { // movement of 2 squares forward from starting position
        if (target == EMPTY_CELL && gameState->board[x + mf][y] == EMPTY_CELL)
            return true;
        else
            return false;
    } else if (z == (x + mf) && abs(w - y) == 1) { // diagonal step forward to capture
        if (isOpponentPiece(player, target))
            return true;
        else
            return false;
    } else
        return false;
}

bool isLegalBishopMovement(SPGameState *gameState, int x, int y, int z, int w) {
    if (abs(x - z) != abs(y - w)) {
        return false;
    }
    char c;
    int x_sign = (z - x) / abs(z - x); // 1 if moving forward,-1 if moving backwards
    int y_sign = (w - y) / abs(w - y); // 1 if moving forward,-1 if moving backwards

    for (int i = 1; i < abs(x - z); i++) { // make sure there is no leap
        c = gameState->board[x + x_sign * i][y + y_sign * i];
        if (c != EMPTY_CELL)
            return false;
    }
    return true;
}

bool isLegalRookMovement(SPGameState *gameState, int x, int y, int z, int w) {
    char c;
    if (x != z && y == w) {
        int x_sign = (z - x) / abs(z - x); // 1 if moving forward,-1 if moving backwards
        for (int i = 1; i < abs(z - x); i++) {
            c = gameState->board[x + x_sign * i][y];
            if (c != EMPTY_CELL)
                return false;
        }
        return true;
    } else if (y != w && x == z) {
        int y_sign = (w - y) / abs(w - y); // 1 if moving forward,-1 if moving backwards
        for (int i = 1; i < abs(w - y); i++) {
            c = gameState->board[x][y + y_sign * i];
            if (c != EMPTY_CELL)
                return false;
        }
        return true;
    } else {
        return false;
    }
}

bool isLegalKnightMovement(int x, int y, int z, int w) {
    if (abs(x - z) == 1 && abs(w - y) == 2)
        return true;
    else if (abs(x - z) == 2 && abs(w - y) == 1)
        return true;
    else
        return false;
}

bool isLegalQueenMovement(SPGameState *gameState, int x, int y, int z, int w) {
    if (isLegalBishopMovement(gameState, x, y, z, w) || isLegalRookMovement(gameState, x, y, z, w))
        return true;
    else
        return false;
}

bool isLegalKingMovement(int x, int y, int z, int w) {
    if (abs(x - z) <= 1 && abs(y - w) <= 1)
        return true;
    return false;
}

bool isLegalMove(SPGameState *gameState, int x, int y, int z, int w, int player) {
    if (!isLegalPieceMovement(gameState, x, y, z, w, player)) {
        return false;
    }

    char source = gameState->board[x][y];
    char target = gameState->board[z][w];
    bool result;

    // now we make the move and check if the player left himself in Check (and if so then the move is illegal)
    gameState->board[x][y] = EMPTY_CELL;
    gameState->board[z][w] = source;
    updateKingLocationIfNecessary(gameState, source, z, w);
    if (isInCheck(gameState, player)) {
        result = false; //move is illegal
    } else {
        result = true;
    }
    //revert changes to the board
    gameState->board[x][y] = source;
    gameState->board[z][w] = target;
    updateKingLocationIfNecessary(gameState, source, x, y);
    return result;
}

SPMoveResult makeMove(SPGameState *gameState, int x, int y, int z, int w) {
    SPMoveResult result = {.success = false, .check = false, .checkmate = false, .tie = false, .msg = NULL};
    if (gameState == NULL) {
        return result;
    } else if (x > 7 || x < 0 || y > 7 || y < 0 || z > 7 || z < 0 || w > 7 || w < 0) {
        result.msg = MSG_INVALID_POSITION;
        return result; //indexes are out of bounds or movement is from a cell to the same cell
    } else if (x == z && y == w) {
        result.msg = MSG_ILLEGAL_MOVE;
        return result;
    }

    char source = gameState->board[x][y];
    char target = gameState->board[z][w];
    if (!isPlayerPiece(gameState->currentPlayer, source)) {
        result.msg = MSG_NO_PIECE_IN_POSITION;
        return result; // player tries to move opponent's piece or empty cell
    } else if (!isLegalMove(gameState, x, y, z, w, gameState->currentPlayer)) {
        result.msg = MSG_ILLEGAL_MOVE;
        return result; // illegal move
    }

    //make the move
    gameState->board[x][y] = EMPTY_CELL;
    gameState->board[z][w] = source;
    result.success = true;

    //update history
    SPMove* move = createMove(x, y, z, w, target);
    spArrayListAddLast(gameState->history, move);

    // change additional data
    updateKingLocationIfNecessary(gameState, source, z, w);
    gameState->currentPlayer = oppositeColorOf(gameState->currentPlayer);

    // test for check, checkmate or tie
    result.check = isInCheck(gameState, gameState->currentPlayer);
    if (isGameOver(gameState)) {
        if (result.check) {
            result.checkmate = true;
            result.msg = gameState->currentPlayer == PLAYER_WHITE ? MSG_BLACK_CHECKMATE : MSG_WHITE_CHECKMATE;
        } else {
            result.tie = true;
            result.msg = MSG_TIE;
        }
    } else if (result.check) {
        result.msg = gameState->currentPlayer == PLAYER_WHITE ? MSG_WHITE_CHECK : MSG_BLACK_CHECK;
    }
    return result;
}

void undoMove(SPGameState *gameState, bool verbose) {
    int index = gameState->history->actualSize - 1;
    SPMove* prevMove = gameState->history->moves[index];
    int z = prevMove->z;
    int w = prevMove->w;
    int x = prevMove->x;
    int y = prevMove->y;

    gameState->board[x][y] = gameState->board[z][w];
    gameState->board[z][w] = prevMove->eatenPiece;
    spArrayListRemoveLast(gameState->history);

    if (gameState->currentPlayer == 1 && verbose) {
        printf(MSG_BLACK_UNDO, z + 1, columnIntToChar(w+1), x+1, columnIntToChar(y+1));
    } else if (verbose) {
        printf(MSG_WHITE_UNDO, z + 1, columnIntToChar(w+1), x+1, columnIntToChar(y+1));
    }
    updateKingLocationIfNecessary(gameState, gameState->board[x][y], x, y);
    gameState->currentPlayer = oppositeColorOf(gameState->currentPlayer);
}
