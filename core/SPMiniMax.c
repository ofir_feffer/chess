//
// created by Ofir & Shawn on 07/09/2017.
//

#include "SPMiniMax.h"

SPMiniMaxNode *createNode(SPGameState *gameState) {
    SPMiniMaxNode *node = malloc(sizeof(SPMiniMaxNode));
    if (node == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    SPGameState *gameStateCopy = copyGameState(gameState);
    if (gameStateCopy == NULL) {
        free(node);
        return NULL;
    }
    node->gameState = gameStateCopy;
    node->move = NULL;
    return node;
}

SPMiniMaxNode *createNodeFromMove(SPGameState *gameState, SPMove* move) {
    SPMiniMaxNode *node = malloc(sizeof(SPMiniMaxNode));
    if (node == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    SPGameState *gameStateCopy = copyGameState(gameState);
    if (gameStateCopy == NULL) {
        free(node);
        return NULL;
    }

    // now make the given move on the copy of the game board
    char source = gameStateCopy->board[move->x][move->y];
    gameStateCopy->board[move->z][move->w] = source;
    gameStateCopy->board[move->x][move->y] = EMPTY_CELL;
    updateKingLocationIfNecessary(gameStateCopy, source, move->z, move->w);
    gameStateCopy->currentPlayer = oppositeColorOf(gameState->currentPlayer);

    node->gameState = gameStateCopy;
    node->move = move;
    return node;
}

SPLinkedList *createChildrenList(SPMiniMaxNode *node) {
    SPLinkedList *resultList = createLinkedList();
    if (resultList == NULL) {
        return NULL;
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            char c = node->gameState->board[i][j];
            if (!isPlayerPiece(node->gameState->currentPlayer, c)) {
                continue;
            }
            SPLinkedList *possibleMoves = getMoves(node->gameState, i, j);
            if (possibleMoves == NULL || possibleMoves->size == 0) {
                free(possibleMoves);
                continue;
            }
            SPNode *listNode = possibleMoves->first;
            while (listNode != NULL) {
                SPTuple *coordinates = (SPTuple *) listNode->data;
                SPMove* move = createMove(i, j, coordinates->x, coordinates->y, '\0');
                SPMiniMaxNode *childNode = createNodeFromMove(node->gameState, move);
                addItemToLinkedList(resultList, childNode);
                listNode = listNode->next;
            }
            destroyLinkedListOfTuples(possibleMoves);
        }
    }

    return resultList;
}

int calculateGameStateScore(SPGameState *gameState, int player, bool* terminalNode) {
    if (isGameOver(gameState)) {
        *terminalNode = true;
        if (isInCheck(gameState, gameState->currentPlayer)) { // game is over by checkmate
            return (player == gameState->currentPlayer) ? INT_MIN : INT_MAX;
        }
    } else {
        *terminalNode = false;
    }

    int score = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            char c = gameState->board[i][j];
            switch (c) {
                case WHITE_PAWN:
                case BLACK_PAWN:
                    score += (isPlayerPiece(player, c) ? 1 : -1);
                    break;
                case WHITE_BISHOP:
                case BLACK_BISHOP:
                    score += (isPlayerPiece(player, c) ? 3 : -3);
                    break;
                case WHITE_ROOK:
                case BLACK_ROOK:
                    score += (isPlayerPiece(player, c) ? 5 : -5);
                    break;
                case WHITE_KNIGHT:
                case BLACK_KNIGHT:
                    score += (isPlayerPiece(player, c) ? 3 : -3);
                    break;
                case WHITE_QUEEN:
                case BLACK_QUEEN:
                    score += (isPlayerPiece(player, c) ? 9 : -9);
                    break;
                case WHITE_KING:
                case BLACK_KING:
                    score += (isPlayerPiece(player, c) ? 100 : -100);
                    break;
            }
        }
    }

    return score;
}

SPMove suggestMove(SPGameState* gameState) {
    SPMove res;
    SPMiniMaxNode* node = createNode(gameState);
    alphaBeta(node, 0, gameState->settings->difficulty, INT_MIN, INT_MAX, true, gameState->currentPlayer, &res);
    destroyMiniMaxNode(node);
    return res;
}

int alphaBeta(SPMiniMaxNode *node, int depth, int maxDepth, int alpha, int beta, bool maximizingPlayer,
              int initiatingPlayer, SPMove *result) {
    bool terminalNode;
    int score = calculateGameStateScore(node->gameState, initiatingPlayer, &terminalNode);
    if (depth == maxDepth || terminalNode) { // terminal node, return its score
        return score;
    }

    if (maximizingPlayer) {
        score = findMaxNode(node, depth, maxDepth, alpha, beta, initiatingPlayer, result);
    } else {
        score = findMinNode(node, depth, maxDepth, alpha, beta, initiatingPlayer);
    }
    return score;
}

int findMaxNode(SPMiniMaxNode *node, int depth, int maxDepth, int alpha, int beta, int initiatingPlayer, SPMove *result) {
    int maxScore = UNINITIALIZED_INT_VALUE;
    SPLinkedList *children = createChildrenList(node);
    if (children == NULL || children->size == 0) { // should never be true, we already checked if the node is terminal
        printf(MSG_NON_TERMINAL_NODE_HAS_NO_CHILDREN);
        return maxScore;
    }

    SPNode *listNode = children->first;
    while (listNode != NULL) {
        SPMiniMaxNode *childNode = (SPMiniMaxNode *) listNode->data;
        int childScore = alphaBeta(childNode, depth + 1, maxDepth, alpha, beta, false, initiatingPlayer, NULL);
        if (maxScore == UNINITIALIZED_INT_VALUE || childScore > maxScore) {
            maxScore = childScore;
            if (depth == 0) {
                copyMove(childNode->move, result);
            }
        }
        destroyMiniMaxNode(childNode);
        alpha = max(alpha, maxScore);
        if (beta <= alpha) {
            destroyRemainingChildren(listNode->next);
            break;
        }
        listNode = listNode->next;
    }

    destroyLinkedList(children);
    return maxScore;
}

int findMinNode(SPMiniMaxNode *node, int depth, int maxDepth, int alpha, int beta, int initiatingPlayer) {
    int minScore = UNINITIALIZED_INT_VALUE;
    SPLinkedList *children = createChildrenList(node);
    if (children == NULL || children->size == 0) { // game is over, check 2 possible cases
        printf(MSG_NON_TERMINAL_NODE_HAS_NO_CHILDREN);
        return minScore;
    }

    SPNode *listNode = children->first;
    while (listNode != NULL) {
        SPMiniMaxNode *childNode = (SPMiniMaxNode *) listNode->data;
        int childScore = alphaBeta(childNode, depth + 1, maxDepth, alpha, beta, true, initiatingPlayer, NULL);
        if (minScore == UNINITIALIZED_INT_VALUE || childScore < minScore) {
            minScore = childScore;
        }
        destroyMiniMaxNode(childNode);
        beta = min(beta, minScore);
        if (beta <= alpha) {
            destroyRemainingChildren(listNode->next);
            break;
        }
        listNode = listNode->next;
    }

    destroyLinkedList(children);
    return minScore;
}

void destroyMiniMaxNode(SPMiniMaxNode* node) {
    if (node == NULL) {
        return;
    }
    destroyGameState(node->gameState);
    if (node->move != NULL) {
        free(node->move);
    }
    free(node);
}

void destroyRemainingChildren(SPNode* listNode)  {
    while (listNode != NULL) {
        SPMiniMaxNode* node = (SPMiniMaxNode *) listNode->data;
        destroyMiniMaxNode(node);
        listNode = listNode->next;
    }
}