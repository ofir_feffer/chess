//
// created by Ofir & Shawn on 22/08/2017.
//

#include "SPGameState.h"
#include "SPXMLFile.h"

SPGameState *createGameStateWithoutHistory(SPSettings *settings) {
    if (!settings) {
        return NULL;
    }
    SPGameState *gameState = malloc(sizeof(SPGameState));
    if (!gameState) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    gameState->settings = settings;
    gameState->currentPlayer = PLAYER_WHITE;
    setupBoard(gameState);
    return gameState;
}

SPGameState *createGameState(SPSettings *settings) {
    SPGameState *gameState = createGameStateWithoutHistory(settings);
    if (gameState == NULL) {
        return NULL;
    }
    gameState->history = spArrayListCreate(6);
    return gameState;
}

SPGameState *createGameStateFromFile(char *path) {
    SPXMLFile *file = loadXMLFromFile(path);
    if (file == NULL) {
        return NULL;
    }

    char gameMode = file->gameMode;
    char difficulty = file->difficulty;
    char userColor = file->userColor;
    char currentPlayer = file->currentTurn;

    if (gameMode < '1' || gameMode > '2' || currentPlayer < '0' || currentPlayer > '1') {
        destroyXMLFile(file);
        return NULL;
    } else if (gameMode == '1' && (difficulty < '1' || difficulty > '5' || userColor < '0' || userColor > '1') ) {
        destroyXMLFile(file);
        return NULL;
    }

    SPSettings *settings = createDefaultSettings();
    if (settings == NULL) {
        destroyXMLFile(file);
        return NULL;
    }

    settings->game_mode = charToInt(gameMode);
    if (gameMode == '1') {
        settings->difficulty = charToInt(difficulty);
        settings->user_color = charToInt(userColor);
    }

    SPGameState *gameState = createGameState(settings);
    if (gameState == NULL) {
        destroySettings(settings);
        destroyXMLFile(file);
        return NULL;
    }
    gameState->currentPlayer = charToInt(currentPlayer);

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            char c = file->rows[i][j];
            if (!isPieceRepresentation(c)) {
                destroyXMLFile(file);
                destroyGameState(gameState);
                return NULL;
            }
            if (c == WHITE_KING) {
                gameState->whiteKingLocation = (SPTuple){.x = i, .y = j};
            } else if (c == BLACK_KING) {
                gameState->blackKingLocation = (SPTuple){.x = i, .y = j};
            }
            gameState->board[i][j] = c;
        }
    }

    destroyXMLFile(file);
    return gameState;
}

SPGameState *copyGameState(SPGameState *source) {
    if (source == NULL) {
        return NULL;
    }
    SPGameState *result = malloc(sizeof(SPGameState));
    if (result == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            result->board[i][j] = source->board[i][j];
        }
    }
    result->settings = copySettings(source->settings);
    result->currentPlayer = source->currentPlayer;
    result->blackKingLocation = source->blackKingLocation;
    result->whiteKingLocation = source->whiteKingLocation;
    result->history = NULL;
    return result;
}

void setupBoard(SPGameState *gameState) {
    gameState->board[0][0] = WHITE_ROOK;
    gameState->board[0][1] = WHITE_KNIGHT;
    gameState->board[0][2] = WHITE_BISHOP;
    gameState->board[0][3] = WHITE_QUEEN;
    gameState->board[0][4] = WHITE_KING;
    gameState->board[0][5] = WHITE_BISHOP;
    gameState->board[0][6] = WHITE_KNIGHT;
    gameState->board[0][7] = WHITE_ROOK;
    gameState->board[7][0] = BLACK_ROOK;
    gameState->board[7][1] = BLACK_KNIGHT;
    gameState->board[7][2] = BLACK_BISHOP;
    gameState->board[7][3] = BLACK_QUEEN;
    gameState->board[7][4] = BLACK_KING;
    gameState->board[7][5] = BLACK_BISHOP;
    gameState->board[7][6] = BLACK_KNIGHT;
    gameState->board[7][7] = BLACK_ROOK;
    for (int j = 0; j < 8; j++) {
        gameState->board[1][j] = WHITE_PAWN;
        gameState->board[6][j] = BLACK_PAWN;
        for (int i = 2; i < 6; i++) {
            gameState->board[i][j] = EMPTY_CELL;
        }
    }
    gameState->whiteKingLocation.x = 0;
    gameState->whiteKingLocation.y = 4;
    gameState->blackKingLocation.x = 7;
    gameState->blackKingLocation.y = 4;
}

bool saveGameStateToFile(SPGameState *gameState, char *path) {
    SPXMLFile *file = createXMLFile();
    if (file == NULL) {
        return false;
    }

    file->gameMode = intToChar(gameState->settings->game_mode);
    file->currentTurn = intToChar(gameState->currentPlayer);
    if (file->gameMode == '1') {
        file->difficulty = intToChar(gameState->settings->difficulty);
        file->userColor = intToChar(gameState->settings->user_color);
    }

    for (int i = 0; i < 8; i++) {
        char *row = malloc(9);
        if (row == NULL) {
            printf(MSG_MALLOC_ERR);
            destroyXMLFile(file);
            return false;
        }
        for (int j = 0; j < 8; j++) {
            row[j] = gameState->board[i][j];
        }
        row[8] = '\0';
        file->rows[i] = row;
    }

    bool result = printXMLToFile(file, path);
    destroyXMLFile(file);
    return result;
}

void printGameBoard(SPGameState *gameState) {
    for (int i = 7; i >= 0; i--) {
        printf("%d| ", i + 1);
        for (int j = 0; j < 8; j++) {
            printf("%c ", gameState->board[i][j]);
        }
        printf("|\n");
    }
    printf("  -----------------\n");
    printf("   A B C D E F G H\n");
}

bool isPCTurn(SPGameState* gameState) {
    if (gameState->settings->game_mode == 1 && gameState->currentPlayer != gameState->settings->user_color) {
        return true;
    }
    return false;
}

void destroyGameState(SPGameState* gameState) {
    if (gameState == NULL) {
        return;
    }
    destroySettings(gameState->settings);
    spArrayListDestroy(gameState->history);
    free(gameState);
}