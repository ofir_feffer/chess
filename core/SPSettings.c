//
// created by Ofir & Shawn on 22/08/2017.
//

#include "SPSettings.h"

SPSettings* createDefaultSettings() {
    SPSettings* settings = (SPSettings*) malloc(sizeof(SPSettings));
    if (settings == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    settings->game_mode = 1;
    settings->difficulty = 2;
    settings->user_color = 1;
    return settings;
}

SPSettings* copySettings(SPSettings* source) {
    if (source == NULL) {
        return NULL;
    }
    SPSettings* res = (SPSettings*) malloc(sizeof(SPSettings));
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->game_mode = source->game_mode;
    res->difficulty = source->difficulty;
    res->user_color = source->user_color;
    return res;
}

bool setGameMode(SPSettings* settings, int mode) {
    if (!settings) {
        return false;
    }
    if (mode != 1 && mode != 2) {
        return false;
    }
    settings->game_mode = mode;
    return true;
}

bool setDifficulty(SPSettings* settings, int difficulty) {
    if (!settings) {
        return false;
    }
    if (difficulty < 1 || difficulty > 4) {
        return false;
    }
    settings->difficulty = difficulty;
    return true;
}

bool setUserColor(SPSettings *settings, int color) {
    if (!settings) {
        return false;
    }
    if (color != 0 && color != 1) {
        return false;
    }
    settings->user_color= color;
    return true;
}

void printSettings(SPSettings* settings) {
    if (!settings) {
        return;
    }

    printf(SETTINGS);
    if (settings->game_mode == 2) {
        printf(GAME_MODE_MSG, 2);
    } else {
        printf(GAME_MODE_MSG, 1);
        printf(DIFFICULTY_MSG, settings->difficulty);
        if (settings->user_color == 0) {
            printf(USER_CLR_MSG, BLACK_MSG);
        } else {
            printf(USER_CLR_MSG, WHITE_MSG);
        }
    }
}

void destroySettings(SPSettings* settings) {
    if (settings != NULL) {
        free(settings);
    }
}
