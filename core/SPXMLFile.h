//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPXMLFILE_H
#define CHESS_SPXMLFILE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "../util/SPMessages.h"

#define CURRENT_TURN_TAG "<current_turn>"
#define GAME_MODE_TAG "<game_mode>"
#define DIFFICULTY_TAG "<difficulty>"
#define USER_CLR_TAG "<user_color>"
#define ROW1 "<row_1>"
#define ROW2 "<row_2>"
#define ROW3 "<row_3>"
#define ROW4 "<row_4>"
#define ROW5 "<row_5>"
#define ROW6 "<row_6>"
#define ROW7 "<row_7>"
#define ROW8 "<row_8>"

#define FILE_FORMAT_PART1 "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<game>\n\
\t<current_turn>%c</current_turn>\n\
\t<game_mode>%c</game_mode>\n"

#define FILE_FORMAT_PART2 "\t<difficulty>%c</difficulty>\n"

#define FILE_FORMAT_PART3 "\t<user_color>%c</user_color>\n"

#define FILE_FORMAT_PART4 "\t<board>\n\
\t\t<row_8>%s</row_8>\n\
\t\t<row_7>%s</row_7>\n\
\t\t<row_6>%s</row_6>\n\
\t\t<row_5>%s</row_5>\n\
\t\t<row_4>%s</row_4>\n\
\t\t<row_3>%s</row_3>\n\
\t\t<row_2>%s</row_2>\n\
\t\t<row_1>%s</row_1>\n\
\t</board>\n\
</game>\n"

typedef struct xmlFile {
    char currentTurn;
    char gameMode;
    char difficulty;
    char userColor;
    char* rows[8];
} SPXMLFile;

/**
 * creates a new SPXMLFile instance and sets all of its attributes to NULL.
 * @return NULL if an allocation error occurs, a SPXMLFile instance otherwise
 */
SPXMLFile* createXMLFile();

/**
 * reads data from a file in the given path,
 * creates a new SPXMLFile instance and sets all of its attributes according to the file's data.
 * @return NULL if an allocation or I/O error occurs, a SPXMLFile instance otherwise
 */
SPXMLFile* loadXMLFromFile(char* path);

/**
 * prints data of a SPXMLFile instance to a file in a given path
 * @param xml - a SPXMLFile instance
 * @param path - path of desired file
 * @return false if source SPXMLFile is NULL or an I/O error occurs, true otherwise
 */
bool printXMLToFile(SPXMLFile *xml, char *path);

/**
 * reads all content of a file in the given path and puts it in a string
 * @param path - path of input file
 * @return NULL if an allocation or I/O error occurs, file's content as string otherwise
 */
char* fileToString(char* path);

/**
 * searches for a tag in a xml string and returns its data.
 * only the first character of the data will be returned.
 * @param content - @NotNULL xml file as a string
 * @param tag - the tag to search for
 * @pre the tag's data, if exists, contains at least 1 characters
 * @return '\0' if the tag was not found in the string, the first character of the tag's data otherwise
 */
char extractCharTag(char* content, char* tag);

/**
 * searches for a tag in a xml string and returns its data.
 * @param content - @NotNULL xml file as a string
 * @param tag - the tag to search for
 * @param length - amount of characters to copy from tag's data
 * @pre the tag's data, if exists, contains at least 'length' characters
 * @return NULL if the tag was not found in the string or an allocation error occurs,
 * the first 'length' characters of the tag's data otherwise
 */
char* extractStringTag(char* content, char* tag, int length);

/**
 * frees all memory resources associated with a SPXMLFile instance.
 * if source SPXMLFile instance is NULL then the function does nothing
 * @param xml - source SPXMLFile instance
 */
void destroyXMLFile(SPXMLFile* xml);

#endif //CHESS_SPXMLFILE_H
