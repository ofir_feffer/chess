//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPGAMESTATE_H
#define CHESS_SPGAMESTATE_H

#include <stdio.h>
#include <stdlib.h>
#include "SPSettings.h"
#include "../util/SPArrayList.h"
#include "../util/SPUtil.h"

typedef struct gameState {
    char board[8][8];
    SPSettings* settings;
    SPArrayList* history;
    int currentPlayer;
    SPTuple whiteKingLocation;
    SPTuple blackKingLocation;
} SPGameState;

/**
 * creates a game state with initial chess placements, and with the given game settings.
 * result game state instance will not have a moves' history list.
 * @param settings - SPSettings instance
 * @return - an instance of the game state
 */
SPGameState* createGameStateWithoutHistory(SPSettings* settings);

/**
 * creates a game state with initial chess placements, and with the given game settings
 * @param settings - SPSettings instance
 * @return - an instance of the game state
 */
SPGameState* createGameState(SPSettings* settings);

/**
 * loads a game from file and creates a game state instance according to the file's data.
 * @param path - the path of the save game
 * @return
 * NULL if an I/O or allocation error occurred or the save file is corrupted. a gameState instance otherwise
 */
SPGameState* createGameStateFromFile(char *path);

/**
 * creates a new gameState with the same parameters as source's parameters.
 * @param gameState - a gameState instance
 * @return NULL if source gameState is NULL or if an allocation error occurs, a copy of the gameState instance otherwise.
 */
SPGameState* copyGameState(SPGameState* source);

/**
 * initializes the board so that the chess pieces are in initial state, the state of the beginning of a new chess game.
 * @param gameState - @NotNULL a gameState instance
 */
void setupBoard(SPGameState *gameState);

/**
 * saves a game to file.
 * @param gameState - @NotNULL a gameState instance
 * @param path - path of desired save game file
 * @return false if an allocation or I/O error occurs, true otherwise
 */
bool saveGameStateToFile(SPGameState* gameState, char *path);

/**
 * prints the state of the board to console
 * @param gameState - @NotNULL a gameState instance
 */
void printGameBoard(SPGameState* gameState);

/**
 * checks whether the gameState game mode is set to single player, and checks if PC's turn.
 * @param gameState - @NotNULL a gameState instance
 * @return true if mode is single player and it is PC's turn, otherwise returns false.
 */
bool isPCTurn(SPGameState* gameState);

/**
 * frees all memory resources that given gameState instance occupies.
 * if source gameState is NULL then the function does nothing
* @param gameState - a gameState instance
 */
void destroyGameState(SPGameState* gameState);

#endif //CHESS_SPGAMESTATE_H
