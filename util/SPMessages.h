//
// created by Ofir & Shawn on 25/08/2017.
//

#ifndef CHESS_SPMESSAGES_H
#define CHESS_SPMESSAGES_H

#define MSG_SPECIFY_SETTING "Specify game setting or type 'start' to begin a game with the current setting:\n"
#define MSG_INVALID_COMMAND "Error: Invalid command\n"
#define MSG_GAME_MODE_SET_1 "Game mode is set to 1 player\n"
#define MSG_GAME_MODE_SET_2 "Game mode is set to 2 players\n"
#define MSG_WRONG_GAME_MODE "Wrong game mode\n"
#define MSG_EXPERT_NOT_SUPPORTED "Expert level not supported, please choose a value between 1 to 4:\n"
#define MSG_WRONG_DIFFICULTY "Wrong difficulty level. The value should be between 1 to 5\n"
#define MSG_FILE_ERR "Error: File doesn't exist or cannot be opened\n"
#define MSG_EXITING "Exiting...\n"
#define MSG_WHITE_ENTER_MOVE "white player - enter your move:\n"
#define MSG_BLACK_ENTER_MOVE "black player - enter your move:\n"
#define MSG_INVALID_POSITION "Invalid position on the board\n"
#define MSG_NO_PIECE_IN_POSITION "The specified position does not contain your piece\n"
#define MSG_ILLEGAL_MOVE "Illegal move\n"
#define MSG_WHITE_CHECK "Check: white King is threatened!\n"
#define MSG_BLACK_CHECK "Check: black King is threatened!\n"
#define MSG_CHECK_PC "Check!\n"
#define MSG_WHITE_CHECKMATE "Checkmate! white player wins the game\n"
#define MSG_BLACK_CHECKMATE "Checkmate! black player wins the game\n"
#define MSG_TIE "The game is tied\n"
#define MSG_SAVE_ERR "File cannot be created or modified\n"
#define MSG_UNDO_UNAVAILABLE "Undo command not available in 2 players mode\n"
#define MSG_EMPTY_HISTORY "Empty history, move cannot be undone\n"
#define MSG_WHITE_UNDO "Undo move for player white : <%d,%c> -> <%d,%c>\n"
#define MSG_BLACK_UNDO "Undo move for player black : <%d,%c> -> <%d,%c>\n"
#define MSG_RESTARTING "Restarting...\n"
#define MSG_PC_MOVE "Computer: move %s at <%d,%c> to <%d,%c>\n"
#define MSG_TIE_PC "The game ends in a tie\n"
#define MSG_MALLOC_ERR "Error: malloc has failed\n"
#define MSG_NO_PATH "Error: command was given no path\n"
#define MSG_UNKNOWN_ARG "Error: unknown argument was given to command\n"

#endif //CHESS_SPMESSAGES_H
