//
// created by Ofir & Shawn on 27/08/2017.
//

#ifndef CHESS_SPLINKEDLIST_H
#define CHESS_SPLINKEDLIST_H

#include <stdlib.h>
#include "SPUtil.h"
#include "SPMessages.h"

// a type used to represent a list node
typedef struct node {
    void* data;
    struct node* next;
} SPNode;

// a type used to represent a linked list
typedef struct list {
    SPNode* first;
    SPNode* last;
    int size;
} SPLinkedList;

/**
 * creates an empty linked list
 * @return
 * NULL if an allocation error occurs,
 * an instance of a linked list otherwise
 */
SPLinkedList* createLinkedList();

/**
 * adds a new node to the head of the list.
 * if the list instance is NULL or an allocation error occurs the function will do nothing
 * @param list - an instance of SPLinkedList
 * @param data - the data to store in the linked list
 */
void addItemToLinkedList(SPLinkedList *list, void *data);

/**
 * merges two linked lists into one.
 * returns an instance of the merged linked list
 * one or both of the given lists will become invalid, and user should use only the returned list
 * @param list1 - an instance of a linked list, NULL is possible
 * @param list2 - an instance of a linked list, NULL is possible
 * @return
 * an instance of the merged linked list
 */
SPLinkedList* mergeLists(SPLinkedList* list1, SPLinkedList* list2);

/**
 * Frees all memory resources associated with the source list.
 * If the source list is NULL, then the function does nothing.
 * @param src - the source linked list
 */
void destroyLinkedList(SPLinkedList* list);

/**
 * Frees all memory resources associated with a source list that contains only Tuples
 * If the source list is NULL, then the function does nothing.
 * @param src - the source linked list, which all of its nodes contain Tuples
 */
void destroyLinkedListOfTuples(SPLinkedList* list);


#endif //CHESS_SPLINKEDLIST_H
