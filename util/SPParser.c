//
// created by Ofir & Shawn on 22/08/2017.
//

#include "SPParser.h"

SP_COMMAND strToCMD(const char* str) {
    if (str == NULL) {
        return SP_INVALID_LINE_CMD;
    } else if (strcmp(str, MOVE) == 0) {
        return SP_MOVE_CMD;
    } else if (strcmp(str, UNDO) == 0) {
        return SP_UNDO_MOVE_CMD;
    } else if (strcmp(str, SAVE) == 0) {
        return SP_SAVE_CMD;
    } else if (strcmp(str, QUIT) == 0) {
        return SP_QUIT_CMD;
    } else if (strcmp(str, RESET) == 0) {
        return SP_RESTART_CMD;
    } else if (strcmp(str, LOAD) == 0) {
        return SP_LOAD_CMD;
    } else if (strcmp(str, GAME_MODE) == 0) {
        return SP_GAME_MODE_CMD;
    } else if (strcmp(str, DIFFICULTY) == 0) {
        return SP_DIFFICULTY_CMD;
    } else if (strcmp(str, USER_COLOR) == 0) {
        return SP_USER_COLOR_CMD;
    } else if (strcmp(str, DEFAULT) == 0) {
        return SP_DEFAULT_CMD;
    } else if (strcmp(str, PRINT_SETTINGS) == 0) {
        return SP_PRINT_SETTINGS_CMD;
    } else if (strcmp(str, START) == 0) {
        return SP_START_CMD;
    } else {
        return SP_INVALID_LINE_CMD;
    }
}

bool isWhiteSpaceCharacter(char c) {
    return c == ' ' || c == '\t' || c == 'r' || c == '\n';
}

void skipWhiteSpace(char** str) {
    (*str)++;
    while (isWhiteSpaceCharacter(**str)) {
        (*str)++;
    }
}

char* removeWhiteSpaces(char* str) {
    if (str == NULL) {
        return NULL;
    }

    char* res = malloc(strlen(str) + 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    if (isWhiteSpaceCharacter(*str)) { // skip leading whitespace
        skipWhiteSpace(&str);
    }

    int i = 0;
    while (*str != '\0') {
        res[i] = *str;
        i++;
        skipWhiteSpace(&str); // skip whitespaces after each copied character
    }
    res[i] = '\0';
    return res;
}

bool extractIntArgumentToVar(char *str, int *var) {
    if (!str || !isInt(str)) {
        return false;
    }
    *var = atoi(str);
    return true;
}

SPCommand parseLine(const char* line) {
    SPCommand command;
    if (!line) {
        command.cmd = SP_INVALID_LINE_CMD;
        return command;
    }

    char* str = malloc(strlen(line) + 1);
    if (str == NULL) {
        printf(MSG_MALLOC_ERR);
        command.cmd = SP_INVALID_LINE_CMD;
        return command;
    }
    strcpy(str, line);
    char *token;

    /* get the first token */
    token = strtok(str, DELIMITER);
    command.cmd = strToCMD(token);
    if (command.cmd == SP_INVALID_LINE_CMD) {
        free(str);
        return command;
    }
    command.isValidArg = true;

    // get second token
    if (command.cmd == SP_MOVE_CMD) {
        token = strtok(NULL, MOVE_CMD_DELIMITER);
        command.isValidMoveCmdStructure = true;
    } else {
        token = strtok(NULL, DELIMITER);
    }

    switch (command.cmd) {
        case SP_UNDO_MOVE_CMD:
        case SP_QUIT_CMD:
        case SP_RESTART_CMD:
        case SP_DEFAULT_CMD:
        case SP_PRINT_SETTINGS_CMD:
        case SP_START_CMD:
            // these commands should not have a second argument
            if (token != NULL) {
                command.isValidArg = false;
            }
            break;

        case SP_SAVE_CMD:
        case SP_LOAD_CMD:
            handleLoadSaveCommandParsing(&command, token);
            break;

        case SP_GAME_MODE_CMD:
        case SP_DIFFICULTY_CMD:
        case SP_USER_COLOR_CMD:
            if (!extractIntArgumentToVar(token, &command.x)) {
                command.isValidArg = false;
            }
            break;

        case SP_MOVE_CMD:
            handleMoveCommandParsing(&command, token);
            break;

        default: //should never get here
            break;

    }

    free(str);
    return command;
}

void handleLoadSaveCommandParsing(SPCommand* command, char* token) {
    if (token == NULL) {
        command->isValidArg = false;
        return;
    }
    command->path = malloc(strlen(token) + 1);
    if (command->path == NULL) {
        printf(MSG_MALLOC_ERR);
        command->isValidArg = false;
        return;
    }
    strcpy(command->path, token);
}

void handleMoveCommandParsing(SPCommand* command, char* token) {
    if (token == NULL) {
        command->isValidMoveCmdStructure = false;
        return;
    }

    // copy first arg
    char* arg1 = malloc(strlen(token) + 1);
    if (arg1 == NULL) {
        printf(MSG_MALLOC_ERR);
        command->cmd = SP_INVALID_LINE_CMD;
        return;
    }
    strcpy(arg1, token);

    // get second arg
    token = strtok(NULL, MOVE_CMD_DELIMITER);
    if (token == NULL) {
        command->isValidMoveCmdStructure = false;
        free(arg1);
        return;
    }

    // copy second arg
    char* arg2 = malloc(strlen(token) + 1);
    if (arg2 == NULL) {
        printf(MSG_MALLOC_ERR);
        command->cmd = SP_INVALID_LINE_CMD;
        free(arg1);
        return;
    }
    strcpy(arg2, token);

    parseMoveCommandArgs(command, arg1, arg2);
    free(arg1);
    free(arg2);
}

void parseMoveCommandArgs(SPCommand *cmd, char *arg1, char *arg2) {
    // check if arguments' structure is valid
    if (!isValidMoveArgumentStructure(arg1) || !isValidMoveArgumentStructure(arg2)) {
        cmd->isValidMoveCmdStructure = false;
        return;
    }

    // parse first argument
    bool parseRes = parseMoveArgumentToVariables(arg1, &(cmd->x), &(cmd->y));
    if (!parseRes) {
        cmd->isValidArg = false;
        return;
    }

    // parse second argument
    parseRes = parseMoveArgumentToVariables(arg2, &(cmd->z), &(cmd->w));
    if (!parseRes) {
        cmd->isValidArg = false;
    }
}

bool isValidMoveArgumentStructure(char *arg) {
    char* argN = removeWhiteSpaces(arg);
    int length = strlen(argN);
    if (argN[0] != '<' || argN[length - 1] != '>') { // check for leading '<' and trailing '>'
        free(argN);
        return false;
    }

    // check for a ',' inside the arg
    while (*arg != '\0') {
        if (*arg == ',') {
            free(argN);
            return true;
        }
        arg++;
    }

    free(argN);
    return false;
}

bool parseMoveArgumentToVariables(char* arg, int* var1, char* var2) {
    if (isWhiteSpaceCharacter(*arg)) { // skip leading whitespace
        skipWhiteSpace(&arg);
    }

    if ((*arg) != '<') { // make sure arg starts with '<'
        return false;
    }

    skipWhiteSpace(&arg);

    //extract row number
    if ((*arg) < '1' || (*arg) > '8') {
        return false;
    }
    *var1 = (*arg) - '0';

    skipWhiteSpace(&arg);

    if ((*arg) != ',') { // make sure indexes are separated by comma
        return false;
    }

    skipWhiteSpace(&arg);

    // extract column character
    if ((*arg) < 'A' || (*arg) > 'H') {
        return false;
    }
    *var2 = (*arg);

    skipWhiteSpace(&arg);

    if ((*arg) != '>') { // make sure arg ends with '>'
        return false;
    }

    skipWhiteSpace(&arg);

    if ((*arg) != '\0') { // make sure there is no other characters after last '>'
        return false;
    }

    return true;
}

