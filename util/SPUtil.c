//
// created by Ofir & Shawn on 24/08/2017.
//

#include "../core/SPGameState.h"
#include "SPUtil.h"

SPTuple* createTuple(int x, int y) {
    SPTuple* res = malloc(sizeof(SPTuple));
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->x = x;
    res->y = y;
    return res;
}

SPMove* createMove(int x, int y, int z, int w, char eatenPiece) {
    SPMove *move = malloc(sizeof(SPMove));
    if (move == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    (*move) = (SPMove) {.x = x, .y = y, .z = z, .w = w, .eatenPiece = eatenPiece};
    return move;
}

void copyMove(SPMove* source, SPMove* target) {
    target->x = source->x;
    target->y = source->y;
    target->z = source->z;
    target->w = source->w;
    target->eatenPiece = source->eatenPiece;
}

bool isInt(const char* str) {
    int i = 0;
    if (str[0] == '-') {
        i++;
    }
    while (str[i] != '\0') {
        if (str[i] < '0' || str[i] > '9') {
            return false;
        }
        i++;
    }
    return true;
}

int abs(int x) {
    if (x < 0) {
        return -x;
    }
    return x;
}

int max(int x, int y) {
    if (x > y)
        return x;
    return y;
}

int min(int x, int y) {
    if (x < y)
        return x;
    return y;
}

int columnCharToInt(char c) {
    if (c >= 'A' && c <= 'H') {
        return c - 'A' + 1;
    }
    return -1;
}

char columnIntToChar(int x) {
    if (x >= 1 && x <= 8) {
        return x + 'A' - 1;
    }
    return '\0';
}

char intToChar(int x) {
    if (x >= 0 && x <= 9) {
        return '0' + x;
    }
    return '\0';
}

int charToInt(char c) {
    if (c >= '0' && c <= '9') {
        return c - '0';
    }
    return -1;
}

char* getUserInput() {
    char* res = malloc(INITIAL_INPUT_SIZE);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    char c;
    int i = 0, size = INITIAL_INPUT_SIZE;
    while ((c = getchar()) != EOF && c != '\n') {
        res[i] = c;
        i++;
        if (i == size) {
            size += INITIAL_INPUT_SIZE;
            res = realloc(res, size);
            if (res == NULL) {
                return NULL;
            }
        }
    }
    res[i] = '\0';
    return res;
}

char* pieceCharToString(char piece) {
    if (piece == BLACK_PAWN || piece == WHITE_PAWN)
        return PAWN_STRING;
    else if (piece == BLACK_KNIGHT || piece == WHITE_KNIGHT)
        return KNIGHT_STRING;
    else if (piece == BLACK_BISHOP || piece == WHITE_BISHOP)
        return BISHOP_STRING;
    else if (piece == BLACK_ROOK || piece == WHITE_ROOK)
        return ROOK_STRING;
    else if (piece == BLACK_QUEEN || piece == WHITE_QUEEN)
        return QUEEN_STRING;
    else if (piece == BLACK_KING || piece == WHITE_KING)
        return KING_STRING;
    else
        return NULL;
}

int oppositeColorOf(int color) {
    return (color + 1) % 2;
}

bool isWhitePiece(char c) {
    switch (c) {
        case WHITE_PAWN:
        case WHITE_BISHOP:
        case WHITE_ROOK:
        case WHITE_KNIGHT:
        case WHITE_QUEEN:
        case WHITE_KING:
            return true;
        default:
            return false;
    }
}

bool isBlackPiece(char c) {
    switch (c) {
        case BLACK_PAWN:
        case BLACK_BISHOP:
        case BLACK_ROOK:
        case BLACK_KNIGHT:
        case BLACK_QUEEN:
        case BLACK_KING:
            return true;
        default:
            return false;
    }
}

bool isPieceRepresentation(char c) {
    return isWhitePiece(c) || isBlackPiece(c) || c == EMPTY_CELL;
}

int colorOfPiece(char c) {
    if (isWhitePiece(c)) {
        return PLAYER_WHITE;
    } else {
        return PLAYER_BLACK;
    }
}

bool isOpponentPiece(int player, char c) {
    if (player == PLAYER_BLACK) {
        return isWhitePiece(c);
    } else {
        return isBlackPiece(c);
    }
}

bool isPlayerPiece(int player, char c) {
    if (player == PLAYER_BLACK) {
        return isBlackPiece(c);
    } else {
        return isWhitePiece(c);
    }
}

int movementFactorOfPawn(int player) {
    if (player == PLAYER_BLACK) {
        return -1;
    } else{
        return 1;
    }
}

int pawnStartingRow(int player) {
    if (player == PLAYER_BLACK) {
        return 6;
    } else {
        return 1;
    }
}

bool isIndexInBounds(int x, int y) {
    return x >= 0 && x <=7 && y >= 0 && y <= 7;
}
