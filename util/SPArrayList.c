/*
 * SPArrayList.c
 *
 *  Created on: 10/06/2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#include "SPArrayList.h"

SPArrayList* spArrayListCreate() {
	SPArrayList* list = (SPArrayList*) malloc(sizeof(SPArrayList));
	if (list == NULL) {
        printf(MSG_MALLOC_ERR);
		return NULL;
	}
	list->actualSize = 0;
	return list;
}

void spArrayListDestroy(SPArrayList* src) {
	if (src == NULL) {
		return;
	}
	for (int i = 0; i < src->actualSize; i++) {
		if (src->moves[i] != NULL) {
			free(src->moves[i]);
		}
	}
	free(src);
}

SP_ARRAY_LIST_MESSAGE spArrayListClear(SPArrayList* src) {
	if (src == NULL) {
		return SP_ARRAY_LIST_INVALID_ARGUMENT;
	}
	for (int i = 0; i < src->actualSize; i++) {
		if (src->moves[i] != NULL) {
			free(src->moves[i]);
		}
	}
	src->actualSize = 0;
	return SP_ARRAY_LIST_SUCCESS;
}

SP_ARRAY_LIST_MESSAGE spArrayListAddLast(SPArrayList* src, SPMove* move) {
	if (src == NULL) {
		return SP_ARRAY_LIST_INVALID_ARGUMENT;
	}
	if (src->actualSize == MAX_SIZE) {
		spArrayListRemoveFirst(src);
	}
	src->moves[src->actualSize] = move;
	src->actualSize++;
	return SP_ARRAY_LIST_SUCCESS;
}

void shitLeft(SPArrayList *src, int from) {
	for (int i = from ; i < src->actualSize - 1; i++) {
		src->moves[i] = src->moves[i+1];
	}
}

SP_ARRAY_LIST_MESSAGE spArrayListRemoveFirst(SPArrayList* src){
	if (src == NULL) {
		return SP_ARRAY_LIST_INVALID_ARGUMENT;
	} else if (src->actualSize == 0) {
		return SP_ARRAY_LIST_EMPTY;
	}
	if (src->moves[0] != NULL) {
		free(src->moves[0]);
	}
	shitLeft(src, 0);
	src->actualSize--;
	return SP_ARRAY_LIST_SUCCESS;
}

SP_ARRAY_LIST_MESSAGE spArrayListRemoveLast(SPArrayList* src){
	if (src == NULL) {
		return SP_ARRAY_LIST_INVALID_ARGUMENT;
	} else if (src->actualSize == 0) {
		return SP_ARRAY_LIST_EMPTY;
	}
	if (src->moves[src->actualSize - 1] != NULL) {
		free(src->moves[src->actualSize - 1]);
	}
	src->actualSize--;
	return SP_ARRAY_LIST_SUCCESS;
}
