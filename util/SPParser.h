//
// created by Ofir & Shawn on 22/08/2017.
//

#ifndef CHESS_SPPARSER_H
#define CHESS_SPPARSER_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "SPUtil.h"
#include "SPMessages.h"

#define MOVE "move"
#define UNDO "undo"
#define SAVE "save"
#define QUIT "quit"
#define RESET "reset"
#define LOAD "load"
#define GAME_MODE "game_mode"
#define DIFFICULTY "difficulty"
#define USER_COLOR "user_color"
#define DEFAULT "default"
#define PRINT_SETTINGS "print_setting"
#define START "start"
#define DELIMITER " \t\r\n"
#define MOVE_CMD_DELIMITER "to"

//a type used to represent a command
typedef enum {
    SP_UNDO_MOVE_CMD,
    SP_MOVE_CMD,
    SP_QUIT_CMD,
    SP_RESTART_CMD,
    SP_INVALID_LINE_CMD,
    SP_SAVE_CMD,
    SP_LOAD_CMD,
    SP_GAME_MODE_CMD,
    SP_DIFFICULTY_CMD,
    SP_USER_COLOR_CMD,
    SP_DEFAULT_CMD,
    SP_PRINT_SETTINGS_CMD,
    SP_START_CMD
} SP_COMMAND;

//a new type that is used to encapsulate a parsed line
typedef struct command_t {
    SP_COMMAND cmd;
    char* path;
    int x;
    char y;
    int z;
    char w;
    bool isValidArg;
    bool isValidMoveCmdStructure; // relevant only for move commands, true iff cmd has the structure "move <.,.> to <.,.>"
} SPCommand;

/**
 * Parses a string. if the string is a valid SP_COMMAND then the relevant SP_COMMAND is returned.
 * otherwise, returns SP_INVALID_LINE
 * @return
 * SP_COMMAND which represents the input string or SP_INVALID_LINE if no SP_COMMAND matches.
 */
SP_COMMAND strToCMD(const char* str);

/**
 *
 * @param c - a character
 * @return
 * true if the character is a white-space character, false otherwise
 */
bool isWhiteSpaceCharacter(char c);

/**
 * gets a pointer to a string and advances the string to its first non white-space character
 * @param str - a pointer to a string
 */
void skipWhiteSpace(char** str);

/**
 * gets a source string and returns a new string which doesn't contain whitespaces
 * @param str - source string
 * @return
 * a new string without whitespaces
 */
char* removeWhiteSpaces(char* str);

/**
 * Gets a string and a pointer to an integer.
 * if the string is an int representation then it's converted to an integer and stored in the variable pointed by the int pointer
 * @param str
 * @param var
 * @return true if string is an integer representation. false otherwise
 */
bool extractIntArgumentToVar(char *str, int *var);

/**
 * Parses a specified line.
 * @return
 * A parsed line such that:
 *   cmd - contains the command type, if the line is invalid then this field is
 *         set to INVALID_LINE
 *   isValidArg - is set to true if the command should get an argument and the argument is valid,
 *                  is set to false if the command got an invalid argument or if the command should get an argument but an argument was not given
 *   x - the integer argument in case validArg is set to true
 *   y - a char argument in case the command is a move command
 *   z - a int argument in case the command is a move command
 *   w - a char argument in case the command is a move command
 *   path - a string that represents a file path for a load or save command
 *   isValidMoveCmdStructure - relevant only for move commands, true iff cmd has the structure "move <.,.> to <.,.>"
 */
SPCommand parseLine(const char* str);

/**
 * Parses a 'load' or 'save' command. if command was not given an argument then isValidArg is set to false,
 * otherwise the argument is copied to the path field of the command
 * @param command - an instance of a command
 * @param token - the argument given to the command
 */
void handleLoadSaveCommandParsing(SPCommand* command, char* token);

/**
 * Parses a 'move' command. the function gets the first argument of the command.
 * if the argument is NULL, then isValidMoveCmdStructure is set to false and the function quits.
 * the function then looks for a second argument. if there's no second argument then isValidMoveCmdStructure is set to false and the function quits.
 * the function then parses both arguments.
 * @param command - an instance of a 'move' command
 * @param token - the first argument that was given to the command
 */
void handleMoveCommandParsing(SPCommand* command, char* token);

/**
 * Parses the two arguments that were given to a 'move' command
 * @param cmd - an instance of a 'move' command
 * @param arg1 - the first argument of the command
 * @param arg2 - the second argument of the command
 */
void parseMoveCommandArgs(SPCommand *cmd, char *arg1, char *arg2);

/**
 * gets an argument of a 'move' command and checks if it has the right structure
 * @param arg - a 'move' command argument
 * @return
 * true if the argument if of the formant "<.,.>", false otherwise
 */
bool isValidMoveArgumentStructure(char *arg);

/**
 * gets a valid argument of a 'move' command, a pointer to an integer and a pointer to a character.
 * the function parses the argument and stores its data in the variables pointed by the given pointers.
 * @pre both pointers are valid
 * @param arg - the string representation of the 'move' command argument
 * @param var1 - a pointer to an integer
 * @param var2 - a pointer to a character
 * @return
 * true if parsing was successful, false otherwise
 */
bool parseMoveArgumentToVariables(char* arg, int* var1, char* var2);


#endif //CHESS_SPPARSER_H
