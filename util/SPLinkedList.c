//
// created by Ofir & Shawn on 27/08/2017.
//

#include "SPLinkedList.h"

SPLinkedList* createLinkedList() {
    SPLinkedList* res = malloc(sizeof(SPLinkedList));
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    res->size = 0;
    res->first = NULL;
    res->last = NULL;
    return res;
}

void addItemToLinkedList(SPLinkedList *list, void *data) {
    if (list == NULL) {
        return;
    }
    SPNode* node = malloc(sizeof(SPNode));
    if (node == NULL) {
        printf(MSG_MALLOC_ERR);
        return;
    }
    node->data = data;
    node->next = list->first;
    list->first = node;
    if (list->last == NULL) {
        list->last = node;
    }
    list->size++;
}

SPLinkedList* mergeLists(SPLinkedList* list1, SPLinkedList* list2) {
    if (list1 == NULL || list1->size == 0) {
        destroyLinkedList(list1);
        return list2;
    } else if (list2 == NULL || list2->size == 0) {
        destroyLinkedList(list2);
        return list1;
    }
    list1->last->next = list2->first;
    list1->last = list2->last;
    list1->size += list2->size;
    free(list2);
    return list1;
}

void destroyLinkedList(SPLinkedList* list) {
    if (list == NULL) {
        return;
    }
    SPNode* node = list->first;
    while (node != NULL) {
        SPNode* temp = node->next;
        free(node);
        node = temp;
    }
    free(list);
}

void destroyLinkedListOfTuples(SPLinkedList* list) {
    if (list == NULL) {
        return;
    }
    SPNode* node = list->first;
    while (node != NULL) {
        SPTuple* tuple = (SPTuple*) node->data;
        if (tuple != NULL) {
            free(tuple);
        }
        SPNode* temp = node->next;
        free(node);
        node = temp;
    }
    free(list);
}