//
// created by Ofir & Shawn on 24/08/2017.
//

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef CHESS_SPUTIL_H
#define CHESS_SPUTIL_H

#define INITIAL_INPUT_SIZE 20
#define PLAYER_BLACK 0
#define PLAYER_WHITE 1
#define WHITE_PAWN 'm'
#define WHITE_BISHOP 'b'
#define WHITE_ROOK 'r'
#define WHITE_KNIGHT 'n'
#define WHITE_QUEEN 'q'
#define WHITE_KING 'k'
#define BLACK_PAWN 'M'
#define BLACK_BISHOP 'B'
#define BLACK_ROOK 'R'
#define BLACK_KNIGHT 'N'
#define BLACK_QUEEN 'Q'
#define BLACK_KING 'K'
#define EMPTY_CELL '_'
#define PAWN_STRING "pawn"
#define BISHOP_STRING "bishop"
#define ROOK_STRING "rook"
#define KNIGHT_STRING "knight"
#define QUEEN_STRING "queen"
#define KING_STRING "king"

// a type used to represent a tuple of 2 integers
typedef struct tuple {
    int x;
    int y;
} SPTuple;


// a type used to represent details of a single game move
typedef struct move {
    int x;
    int y;
    int z;
    int w;
    char eatenPiece; // could be EMPTY_CELL as well
} SPMove;

/**
 * creates a tuple with given integers
 * @param x - an integer
 * @param y - an integer
 * @return
 * NULL if an allocation error occurs, a tuple instance otherwise
 */
SPTuple* createTuple(int x, int y);

/**
 * creates a SPMove instance with given parameters
 * @param x - row index of source cell
 * @param y  - column index of source cell
 * @param z - row index of target cell
 * @param w - column index of target cell
 * @param eatenPiece - the data in the target cell before the move was made
 * @return
 * NULL if an allocation error occurs, a SPMOVE instance otherwise
 */
SPMove* createMove(int x, int y, int z, int w, char eatenPiece);

/**
 * copy dota from source SPMove to target SPMove
 * @pre source and target are non NULL and valid
 * @param source - source SPMove
 * @param target - target SPMove
 */
void copyMove(SPMove* source, SPMove* target);

/**
 * Checks if a specified string represents a valid integer.
 *@param str - non NULL source string
 * @return
 * true if the string represents a valid integer, and false otherwise.
 */
bool isInt(const char* str);

/**
 * @param x - an integer
 * @return
 * the absolute value of the integer
 */
int abs(int x);

/**
 *
 * @param x - an integer
 * @param y - an integer
 * @return
 * the maximum of both integers
 */
int max(int x, int y);

/**
 *
 * @param x - an integer
 * @param y - an integer
 * @return
 * the minimum of both integers
 */
int min(int x, int y);

/**
 * Checks if a character is a capital letter in range A-H, and converts it to a matching integer index.
 * returns conversion of the character to an integer in range 1-8,
 * or -1 if character is not in range A-H
 */
int columnCharToInt(char c);

/**
 *
 * @param x - an integer
 * @return
 * '\0' if x is not in range 1-8, or a conversion of the integer to a character in range A-H otherwise
 */
char columnIntToChar(int x);

/**
 *
 * @param x - an integer
 * @return
 * '\0' is x is not in range 1-9, or a conversion of the integer to a character otherwise
 */
char intToChar(int x);

/**
 *
 * @param c - a character
 * @return
 * -1 if c is not in range '1'-'9', or a conversion of the char to an integer otherwise
 */
int charToInt(char c);

/**
 * reads input from stdin and stores in a string
 * @return
 * a string with the input from stdin
 */
char* getUserInput();

/**
 *
 * @param piece - a representation of a chess piece
 * @return
 * NULL if the character is not a valid representation of a chess piece, or a string representation of the given piece otherwise.
 * i.e. for input 'q' the function will return the string 'queen' etc.
 */
char* pieceCharToString(char piece);

/**
 * gets an integer which represents a player color (0 for black, 1 for white),
 * returns the integer representation of the opposite color
 */
int oppositeColorOf(int color);

/**
 *
 * @param c - a representation of a chess piece
 * @return
 * true if the piece is of the white player, false otherwise
 */
bool isWhitePiece(char c);

/**
 *
 * @param c - a representation of a chess piece
 * @return
 * true if the piece is of the black player, false otherwise
 */
bool isBlackPiece(char c);

/**
 *
 * @param c - a character
 * @return
 * true if the character is a valid representation of a chess piece ('_' is valid as well), false otherwise
 */
bool isPieceRepresentation(char c);

/**
 * @pre c is a valid representation of a chess piece
 * @param c - a valid representation of a chess piece
 * @return
 * 1 if the piece is of the white player, 0 otherwise
 */
int colorOfPiece(char c);

/**
 *
 * @param player - an integer representation of a chess player (0 for black, 1 for white)
 * @param c - a valid representation of a chess piece
 * @return
 * true if the chess piece belongs to the opponent
 */
bool isOpponentPiece(int player, char c);

/**
 *
 * @param player - an integer representation of a chess player (0 for black, 1 for white)
 * @param c - a valid representation of a chess piece
 * @return
 * true if the chess piece belongs to the given player
 */
bool isPlayerPiece(int player, char c);

/**
 *
 * @param player - an integer representation of a chess player (0 for black, 1 for white)
 * @return
 * -1 for black player, 1 for white player
 */
int movementFactorOfPawn(int player);

/**
 * returns the starting row index of a player's pawns
 * @param player - an integer representation of a chess player (0 for black, 1 for white)
 * @return
 * 6 for black player, 1 for white player
 */
int pawnStartingRow(int player);

/**
 * checks if given indexes are in a chess board bounds
 * @param x - an integer
 * @param y - an integer
 * @return
 * true if both x and y are in range 0-7, false otherwise
 */
bool isIndexInBounds(int x, int y);

#endif //CHESS_SPUTIL_H
