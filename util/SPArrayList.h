#ifndef SPARRAYLIST_H_
#define SPARRAYLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "SPUtil.h"
#include "SPMessages.h"
#define MAX_SIZE 6

typedef struct sp_array_list_t {
	SPMove* moves[MAX_SIZE];
	int actualSize;
} SPArrayList;

/**
 * A type used for errors
 */
typedef enum sp_array_list_message_t {
	SP_ARRAY_LIST_SUCCESS,
	SP_ARRAY_LIST_INVALID_ARGUMENT,
	SP_ARRAY_LIST_FULL,
	SP_ARRAY_LIST_EMPTY
} SP_ARRAY_LIST_MESSAGE;

/**
 *  Creates an empty array list of game moves.
 *  @return
 *  NULL, if an allocation error occurred, an instance of an array list otherwise.
 */
SPArrayList* spArrayListCreate();

/**
 * Frees all memory resources associated with the source array list. If the
 * source array is NULL, then the function does nothing.
 * @param src - the source array list
 */
void spArrayListDestroy(SPArrayList* src);

/**
 * Clears all elements from the source array list. After invoking this function,
 * the size of the source list will be reduced to zero.
 * @param src - the source array list
 * @return
 * SP_ARRAY_LIST_INVALID_ARGUMENT if src == NULL
 * SP_ARRAY_LIST_SUCCESS otherwise
 */
SP_ARRAY_LIST_MESSAGE spArrayListClear(SPArrayList* src);

/**
 * Inserts an element at the end of the source list. If the array list
 * reached its maximum capacity the first element in the array will be removed,
 * and all elements will be shifted to the left.
 * @param src   - the source array list
 * @param move  - the new move to be inserted
 * @return
 * SP_ARRAY_LIST_INVALID_ARGUMENT - if src == NULL
 * SP_ARRAY_LIST_SUCCESS - otherwise
 */
SP_ARRAY_LIST_MESSAGE spArrayListAddLast(SPArrayList* src, SPMove* move);

/**
 * Shifts left all entries of an ArrayList instance, starting at the 'from' index
 * @param src - SPArrayList instance
 * @param from - index from which to start shifting left
 */
void shitLeft(SPArrayList *src, int from);

/**
 * Removes an element from the beginning of the list.
 * The elements will be shifted to keep the list continuous. If the
 * array list is empty then an error message is returned and the source list
 * is not affected
 * @param src   - The source array list
 * @param move  - The new move to be inserted
 * @return
 * SP_ARRAY_LIST_INVALID_ARGUMENT - if src == NULL
 * SP_ARRAY_LIST_EMPTY - if the source array list is empty
 * SP_ARRAY_LIST_SUCCESS - otherwise
 */
SP_ARRAY_LIST_MESSAGE spArrayListRemoveFirst(SPArrayList* src);

/**
 * Removes an element from the end of the list. If the
 * array list is empty then an error message is returned and the source list
 * is not affected
 * @param src   - The source array list
 * @param move  - The new move to be inserted
 * @return
 * SP_ARRAY_LIST_INVALID_ARGUMENT - if src == NULL
 * SP_ARRAY_LIST_EMPTY - if the source array list is empty
 * SP_ARRAY_LIST_SUCCESS - otherwise.
 */
SP_ARRAY_LIST_MESSAGE spArrayListRemoveLast(SPArrayList* src);


#endif
