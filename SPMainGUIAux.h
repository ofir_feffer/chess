//
// created by Ofir & Shawn on 12/09/2017.
//

#include <SDL.h>
#include "gui/SPGuiManager.h"
#include "core/SPMiniMax.h"

#ifndef CHESS_SPMAINGUIAUX_H
#define CHESS_SPMAINGUIAUX_H

/**
 * gets a gui manager instance. if the active window is a game window and it's also the PC's turn to make a move,
 * then the function will make the PC's move using the minimax algorithm. otherwise, the function does nothing
 * @param guiManager - a GUI manager instance
 */
void guiMakePCMove(SPGuiManager *guiManager);

/**
 * initializes SDL and the GUI, awaits window events in a loop and acts according to them.
 * @return
 * the exit code, 0 for successful run and 1 in case of failure
 */
int startGUI();

#endif //CHESS_SPMAINGUIAUX_H
