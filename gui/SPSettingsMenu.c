/*
 * SPSettingsMenu.c
 *
 *  Created on: Sep 11, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#include "SPSettingsMenu.h"

const SDL_Rect gameModeLabelR = {.x = 282, .y = 59, .h = 54, .w = 260};
const SDL_Rect onePlayerR = {.x = 238, .y = 118, .h = 43, .w = 160};
const SDL_Rect twoPlayersR = {.x = 421, .y = 118, .h = 43, .w = 160};
const SDL_Rect difficultyLabelR = {.x = 282, .y = 261, .h = 54, .w = 260};
const SDL_Rect noobR = {.x = 54, .y = 328, .h = 43, .w = 160};
const SDL_Rect easyR = {.x = 238, .y = 328, .h = 43, .w = 160};
const SDL_Rect moderateR = {.x = 421, .y = 328, .h = 43, .w = 160};
const SDL_Rect hardR = {.x = 599, .y = 328, .h = 43, .w = 160};
const SDL_Rect colorLabelR = {.x = 282, .y = 467, .h = 54, .w = 260};
const SDL_Rect whiteR = {.x = 238, .y = 524, .h = 43, .w = 160};
const SDL_Rect blackR = {.x = 421, .y = 524, .h = 43, .w = 160};
const SDL_Rect startR = {.x = 238, .y = 715, .h = 43, .w = 160};
const SDL_Rect backR = {.x = 421, .y = 715, .h = 43, .w = 160};

SPSettingsWin *settingsWindowCreate() {
    SPSettingsWin *res = (SPSettingsWin *) calloc(sizeof(SPSettingsWin), 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    
    res->window = createWindow(800, 800);
    if (res->window == NULL) {
        free(res);
        return NULL;
    }

    res->gameSettings = createDefaultSettings();
    if (res->gameSettings == NULL) {
        settingsWindowDestroy(res);
        return NULL;
    }

    bool ok = createLabelsTextures(res);
    if (!ok) {
        settingsWindowDestroy(res);
        return NULL;
    }

    ok = createGameModesButtons(res);
    if (!ok) {
        settingsWindowDestroy(res);
        return NULL;
    }

    ok = createDifficultiesButtons(res);
    if (!ok) {
        settingsWindowDestroy(res);
        return NULL;
    }

    ok = createUserColorTextures(res);
    if (!ok) {
        settingsWindowDestroy(res);
        return NULL;
    }

    res->startTexture = createTexture(res->window->renderer, START_BUTTON_PATH);
    res->backTexture = createTexture(res->window->renderer, BACK_BUTTON_PATH);
    if (res->startTexture == NULL || res->backTexture == NULL) {
        settingsWindowDestroy(res);
        return NULL;
    }

    return res;
}

bool createLabelsTextures(SPSettingsWin *win) {
    win->gameModeLabelTexture = createTexture(win->window->renderer, GAME_MODE_LABEL_PATH);
    if (win->gameModeLabelTexture == NULL) {
        return false;
    }

    win->difficultyLabelTexture = createTexture(win->window->renderer, DIFFICULTY_LABEL_PATH);
    if (win->difficultyLabelTexture == NULL) {
        return false;
    }

    win->colorLabelTexture = createTexture(win->window->renderer, USER_COLOR_LABEL_PATH);
    if (win->colorLabelTexture == NULL) {
        return false;
    }
    return true;
}

bool createGameModesButtons(SPSettingsWin *win) {
    win->onePlayerTexture = createTexture(win->window->renderer, ONE_PLAYER_BUTTON_PATH);
    if (win->onePlayerTexture == NULL) {
        return false;
    }

    win->onePlayerInactiveTexture = createTexture(win->window->renderer, ONE_PLAYER_BUTTON_INACTIVE_PATH);
    if (win->onePlayerInactiveTexture == NULL) {
        return false;
    }

    win->twoPlayerTexture = createTexture(win->window->renderer, TWO_PLAYERS_BUTTON);
    if (win->twoPlayerTexture == NULL) {
        return false;
    }

    win->twoPlayerInactiveTexture = createTexture(win->window->renderer, TWO_PLAYER_BUTTON_INACTIVE_PATH);
    if (win->twoPlayerInactiveTexture == NULL) {
        return false;
    }
    return true;
}

bool createDifficultiesButtons(SPSettingsWin *win) {
    win->noobTexture = createTexture(win->window->renderer, NOOB_BUTTON_PATH);
    if (win->noobTexture == NULL) {
        return false;
    }

    win->noobInactiveTexture = createTexture(win->window->renderer, NOOB_BUTTON_INACTIVE_PATH);
    if (win->noobInactiveTexture == NULL) {
        return false;
    }

    win->easyTexture = createTexture(win->window->renderer, EASY_BUTTON_PATH);
    if (win->easyTexture == NULL) {
        return false;
    }

    win->easyInactiveTexture = createTexture(win->window->renderer, EASY_BUTTON_INACTIVE_PATH);
    if (win->easyInactiveTexture == NULL) {
        return false;
    }

    win->moderateTexture = createTexture(win->window->renderer, MODERATE_BUTTON_PATH);
    if (win->moderateTexture == NULL) {
        return false;
    }

    win->moderateInactiveTexture = createTexture(win->window->renderer, MODERATE_BUTTON_INACTIVE_PATH);
    if (win->moderateInactiveTexture == NULL) {
        return false;
    }

    win->hardTexture = createTexture(win->window->renderer, HARD_BUTTON_PATH);
    if (win->hardTexture == NULL) {
        return false;
    }

    win->hardInactiveTexture = createTexture(win->window->renderer, HARD_BUTTON_INACTIVE_PATH);
    if (win->hardInactiveTexture == NULL) {
        return false;
    }
    return true;
}

bool createUserColorTextures(SPSettingsWin *win) {
    win->whiteTexture = createTexture(win->window->renderer, WHITE_BUTTON_PATH);
    if (win->whiteTexture == NULL) {
        return false;
    }

    win->whiteInactiveTexture = createTexture(win->window->renderer, WHITE_BUTTON_INACTIVE_PATH);
    if (win->whiteInactiveTexture == NULL) {
        return false;
    }

    win->blackTexture = createTexture(win->window->renderer, BLACK_BUTTON_PATH);
    if (win->blackTexture == NULL) {
        return false;
    }

    win->blackInactiveTexture = createTexture(win->window->renderer, BLACK_BUTTON_INACTIVE_PATH);
    if (win->blackInactiveTexture == NULL) {
        return false;
    }
    return true;
}

void settingsWindowDestroy(SPSettingsWin *src) {
    if (!src) {
        return;
    }
    destroySettings(src->gameSettings);
    if (src->onePlayerTexture != NULL) {
        SDL_DestroyTexture(src->onePlayerTexture);
    }
    if (src->onePlayerInactiveTexture != NULL) {
        SDL_DestroyTexture(src->onePlayerInactiveTexture);
    }
    if (src->twoPlayerTexture != NULL) {
        SDL_DestroyTexture(src->twoPlayerTexture);
    }
    if (src->twoPlayerInactiveTexture != NULL) {
        SDL_DestroyTexture(src->twoPlayerInactiveTexture);
    }
    if (src->noobTexture != NULL) {
        SDL_DestroyTexture(src->noobTexture);
    }
    if (src->noobInactiveTexture != NULL) {
        SDL_DestroyTexture(src->noobInactiveTexture);
    }
    if (src->easyTexture != NULL) {
        SDL_DestroyTexture(src->easyTexture);
    }
    if (src->easyInactiveTexture != NULL) {
        SDL_DestroyTexture(src->easyInactiveTexture);
    }
    if (src->moderateTexture != NULL) {
        SDL_DestroyTexture(src->moderateTexture);
    }
    if (src->moderateInactiveTexture != NULL) {
        SDL_DestroyTexture(src->moderateInactiveTexture);
    }
    if (src->hardTexture != NULL) {
        SDL_DestroyTexture(src->hardTexture);
    }
    if (src->hardInactiveTexture != NULL) {
        SDL_DestroyTexture(src->hardInactiveTexture);
    }
    if (src->whiteTexture != NULL) {
        SDL_DestroyTexture(src->whiteTexture);
    }
    if (src->whiteInactiveTexture != NULL) {
        SDL_DestroyTexture(src->whiteInactiveTexture);
    }
    if (src->blackTexture != NULL) {
        SDL_DestroyTexture(src->blackTexture);
    }
    if (src->blackInactiveTexture != NULL) {
        SDL_DestroyTexture(src->blackInactiveTexture);
    }
    if (src->startTexture != NULL) {
        SDL_DestroyTexture(src->startTexture);
    }
    if (src->backTexture != NULL) {
        SDL_DestroyTexture(src->backTexture);
    }
    if (src->gameModeLabelTexture != NULL) {
        SDL_DestroyTexture(src->gameModeLabelTexture);
    }
    if (src->difficultyLabelTexture != NULL) {
        SDL_DestroyTexture(src->difficultyLabelTexture);
    }
    if (src->colorLabelTexture != NULL) {
        SDL_DestroyTexture(src->colorLabelTexture);
    }
    
    destroyWindow(src->window);
    free(src);
}

void settingsWindowDraw(SPSettingsWin *src) {
    if (src == NULL) {
        return;
    }
    SDL_SetRenderDrawColor(src->window->renderer, 238, 238, 210, 255);
    SDL_RenderClear(src->window->renderer);
    SDL_RenderCopy(src->window->renderer, src->gameModeLabelTexture, NULL, &gameModeLabelR);
    if (src->gameSettings->game_mode == 2) {
        SDL_RenderCopy(src->window->renderer, src->onePlayerInactiveTexture, NULL, &onePlayerR);
        SDL_RenderCopy(src->window->renderer, src->twoPlayerTexture, NULL, &twoPlayersR);

    } else {
        SDL_RenderCopy(src->window->renderer, src->onePlayerTexture, NULL, &onePlayerR);
        SDL_RenderCopy(src->window->renderer, src->twoPlayerInactiveTexture, NULL, &twoPlayersR);

        SDL_RenderCopy(src->window->renderer, src->difficultyLabelTexture, NULL, &difficultyLabelR);
        if (src->gameSettings->difficulty == 1) {
            SDL_RenderCopy(src->window->renderer, src->noobTexture, NULL, &noobR);
        } else {
            SDL_RenderCopy(src->window->renderer, src->noobInactiveTexture, NULL, &noobR);
        }
        if (src->gameSettings->difficulty == 2) {
            SDL_RenderCopy(src->window->renderer, src->easyTexture, NULL, &easyR);
        } else {
            SDL_RenderCopy(src->window->renderer, src->easyInactiveTexture, NULL, &easyR);
        }
        if (src->gameSettings->difficulty == 3) {
            SDL_RenderCopy(src->window->renderer, src->moderateTexture, NULL, &moderateR);
        } else {
            SDL_RenderCopy(src->window->renderer, src->moderateInactiveTexture, NULL, &moderateR);
        }
        if (src->gameSettings->difficulty == 4) {
            SDL_RenderCopy(src->window->renderer, src->hardTexture, NULL, &hardR);
        } else {
            SDL_RenderCopy(src->window->renderer, src->hardInactiveTexture, NULL, &hardR);
        }

        SDL_RenderCopy(src->window->renderer, src->colorLabelTexture, NULL, &colorLabelR);
        if (src->gameSettings->user_color == 0) {
            SDL_RenderCopy(src->window->renderer, src->whiteInactiveTexture, NULL, &whiteR);
            SDL_RenderCopy(src->window->renderer, src->blackTexture, NULL, &blackR);
        } else {
            SDL_RenderCopy(src->window->renderer, src->whiteTexture, NULL, &whiteR);
            SDL_RenderCopy(src->window->renderer, src->blackInactiveTexture, NULL, &blackR);
        }
    }

    SDL_RenderCopy(src->window->renderer, src->startTexture, NULL, &startR);
    SDL_RenderCopy(src->window->renderer, src->backTexture, NULL, &backR);
    SDL_RenderPresent(src->window->renderer);
}

void settingWindowHide(SPSettingsWin *src) {
    SDL_HideWindow(src->window->sdl_window);
}

void settingsWindowShow(SPSettingsWin *src) {
    SDL_ShowWindow(src->window->sdl_window);
}

bool isClickOnOnePlayer(int x, int y) {
    return isInRectangle(x, y, onePlayerR);
}

bool isClickOnTwoPlayers(int x, int y) {
    return isInRectangle(x, y, twoPlayersR);
}

bool isClickOnNoob(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, noobR);
}

bool isClickOnEasy(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, easyR);
}

bool isClickOnModerate(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, moderateR);
}

bool isClickOnHard(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, hardR);
}

bool isClickOnBlack(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, blackR);
}

bool isClickOnWhite(int x, int y, SPSettingsWin* window) {
    return window->gameSettings->game_mode == 1 && isInRectangle(x, y, whiteR);
}

bool isClickOnBack(int x, int y) {
    return isInRectangle(x, y, backR);
}

bool isClickOnStart(int x, int y) {
    return isInRectangle(x, y, startR);
}

SP_EVENT settingsWindowHandleEvent(SPSettingsWin *window, SDL_Event *event) {
    if (!event) {
        return INVALID_EVENT;
    }
    switch (event->type) {
        case SDL_MOUSEBUTTONUP:
            if (isClickOnOnePlayer(event->button.x, event->button.y)) {
                window->gameSettings->game_mode = 1;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnTwoPlayers(event->button.x, event->button.y)) {
                window->gameSettings->game_mode = 2;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnNoob(event->button.x, event->button.y, window)) {
                window->gameSettings->difficulty = 1;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnEasy(event->button.x, event->button.y, window)) {
                window->gameSettings->difficulty = 2;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnModerate(event->button.x, event->button.y, window)) {
                window->gameSettings->difficulty = 3;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnHard(event->button.x, event->button.y, window)) {
                window->gameSettings->difficulty = 4;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnWhite(event->button.x, event->button.y, window)) {
                window->gameSettings->user_color = 1;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnBlack(event->button.x, event->button.y, window)) {
                window->gameSettings->user_color = 0;
                return EVENT_CHANGED_SETTINGS;
            } else if (isClickOnBack(event->button.x, event->button.y)) {
                return EVENT_BACK;
            } else if (isClickOnStart(event->button.x, event->button.y)) {
                return EVENT_START_GAME;
            }
            break;

        case SDL_WINDOWEVENT:
            if (event->window.event == SDL_WINDOWEVENT_CLOSE) {
                return EVENT_QUIT;
            }
            break;

        default:
            return EVENT_NONE;
    }


    return EVENT_NONE;
}


