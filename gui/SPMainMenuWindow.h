//
// created by Ofir & Shawn on 11/09/2017.
//

#ifndef CHESS_SPMAINMENU_H
#define CHESS_SPMAINMENU_H

#include <stdbool.h>
#include <SDL.h>
#include <stdio.h>
#include "SPWindow.h"
#include "../guiUtil/SPGuiUtil.h"
#include "../guiUtil/SPGameSlotsManager.h"
#include "../guiUtil/SPFilsPaths.h"

typedef struct {
    SPWindow* window;
    SDL_Texture *exitTexture;
    SDL_Texture *newGameTexture;
    SDL_Texture *loadTexture;
    SDL_Texture *loadInactiveTexture;
    SPGameSlotsManager *gameSlotsManager;
} SPMainMenuWin;

/**
 * creates a main menu window and initializes all textures needed
 * @return
 * SPMainMenuWin instance.
 */
SPMainMenuWin *mainWindowCreate();

/**
 * draws a main menu window.
 * @param src - main menu window instance
 */
void mainWindowDraw(SPMainMenuWin *src);

/**
 * Frees all memory resources associated with a main menu window instance.
 * if the source window is NULL then the function does nothing
 * @param src - main menu window instance
 */
void mainWindowDestroy(SPMainMenuWin *src);

/**
 * hides a main menu window
 * @param src - @NotNULL main menu window instance
 */
void mainWindowHide(SPMainMenuWin *src);

/**
 * shows a main menu window
 * @param src - @NotNULL main menu window instance
 */
void mainWindowShow(SPMainMenuWin *src);

/**
 * gets a main menu window instance and returns true if there are any saved game slots to load
 * @param win - @NotNULL main menu window instance
 * @return
 * true if there are any saved game slots to load, false otherwise
 */
bool isLoadButtonAvailable(SPMainMenuWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the load button AND load button is active.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param win - @NotNULL main menu window instance
 * @return
 * true if (x,y) is in the rectangle of the load button AND load button is active, false otherwise
 */
bool isClickOnLoad(int x, int y, SPMainMenuWin* window);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the exit button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'exit' button, false otherwise
 */
bool isClickOnExit(int x, int y);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the New Game button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'New Game' button, false otherwise
 */
bool isClickOnNewGame(int x, int y);

/**
 * gets a main menu window instance and a SDL_Event.
 * changes the window state according to the event's type and parameters
 * @param window - @NotNULL main menu window instance
 * @param event - a window or mouse event
 * @return
 * SP_EVENT relevant to the change to the window's state:
 *      EVENT_QUIT if user clicked on exit button
 *      EVENT_NEW_GAME is user clicked on new game button
 *      EVENT_LOAD_MENU if user clicked on load button
 *      EVENT_NONE otherwise
 */
SP_EVENT mainWindowHandleEvent(SPMainMenuWin *win, SDL_Event *event);


#endif //CHESS_SPMAINMENU_H
