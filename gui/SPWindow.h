//
// created by Ofir & Shawn on 21/09/2017.
//

#include <stdio.h>
#include <SDL.h>
#include "../util/SPMessages.h"

#ifndef CHESS_SPWINDOW_H
#define CHESS_SPWINDOW_H

#define DEFAULT_TITLE "Chess"
#define ERROR_WINDOW_CREATION "Error: Could not create window: %s\n"

typedef struct spWindow {
    SDL_Window *sdl_window;
    SDL_Renderer *renderer;
} SPWindow;


/**
 * creates a new SPWindow instance,
 * initializes a SDL_Window with the specified width and height, and initializes a SDL_Renderer.
 * @param width - desired window's width
 * @param height - desired window's height
 * @return
 * SPWindow instance, or NULL if an allocation error occurred
 */
SPWindow* createWindow(int width, int height);

/**
 * frees all memory resources associated with a SPWindow instance.
 * if source SPWindow is NULL the function does nothing
 * @param src  - source SPWindow
 */
void destroyWindow(SPWindow* src);

#endif //CHESS_SPWINDOW_H

