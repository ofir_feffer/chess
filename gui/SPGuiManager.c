//
// created by Ofir & Shawn on 11/09/2017.
//

#include "SPGuiManager.h"

SPGuiManager *guiManagerCreate() {
    SPGuiManager *res = calloc(sizeof(SPGuiManager), 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->gameSlotsManager = getGameSlotsManagerInstance();
    if (res->gameSlotsManager == NULL) {
        guiManagerDestroy(res);
        return NULL;
    }

    res->mainMenuWin = mainWindowCreate();
    if (res->mainMenuWin == NULL) {
        free(res);
        return NULL;
    }
    res->activeWin = MAIN_MENU_ACTIVE;
    return res;
}

void guiManagerDestroy(SPGuiManager *src) {
    if (src == NULL) {
        return;
    }
    mainWindowDestroy(src->mainMenuWin);
    settingsWindowDestroy(src->settingsWin);
    slotWindowDestroy(src->loadGameWin);
    gameWindowDestroy(src->gameBoardWin);
    destroyGameSlotsManager(src->gameSlotsManager);
    free(src);
}

void guiManagerDraw(SPGuiManager *src) {
    switch (src->activeWin) {
        case MAIN_MENU_ACTIVE:
            mainWindowShow(src->mainMenuWin);
            mainWindowDraw(src->mainMenuWin);
            break;
        case SETTINGS_WINDOW_ACTIVE:
            if (src->settingsWin == NULL) {
                src->settingsWin = settingsWindowCreate();
            }
            settingsWindowShow(src->settingsWin);
            settingsWindowDraw(src->settingsWin);
            break;
        case LOAD_MENU_ACTIVE:
            if (src->loadGameWin == NULL) {
                src->loadGameWin = slotWindowCreate();
            }
            slotWindowShow(src->loadGameWin);
            slotWindowDraw(src->loadGameWin);
            break;
        case GAME_WINDOW_ACTIVE:
            gameWindowShow(src->gameBoardWin);
            gameWindowDraw(src->gameBoardWin);
            break;
    }
}

SP_EVENT guiManagerHandleEvent(SPGuiManager *src, SDL_Event *event) {
    SP_EVENT res = EVENT_NONE;
    switch (src->activeWin) {
        case MAIN_MENU_ACTIVE:
            res = mainWindowHandleEvent(src->mainMenuWin, event);
            handleManagerDueToMainMenuEvent(src, &res);
            break;
        case SETTINGS_WINDOW_ACTIVE:
            res = settingsWindowHandleEvent(src->settingsWin, event);
            handleManagerDueToSettingsEvent(src, &res);
            break;
        case LOAD_MENU_ACTIVE:
            res = slotWindowHandleEvent(src->loadGameWin, event);
            handleManagerDueToLoadEvent(src, &res);
            break;
        case GAME_WINDOW_ACTIVE:
            res = gameWindowHandleEvent(src->gameBoardWin, event);
            handleManagerDueToGameBoardEvent(src, &res);
            break;
    }
    return res;
}

void handleManagerDueToGameBoardEvent(SPGuiManager *src, SP_EVENT *event) {
    if ((*event) == EVENT_MAIN_MENU) {
        gameWindowHide(src->gameBoardWin);
        gameWindowDestroy(src->gameBoardWin);
        src->gameBoardWin = NULL;
        src->activeWin = MAIN_MENU_ACTIVE;

    }  else if ((*event) == EVENT_LOAD_MENU) {
        gameWindowHide(src->gameBoardWin);
        src->activeWin = LOAD_MENU_ACTIVE;
        src->prevWin = GAME_WINDOW_ACTIVE;
    }
}

void handleManagerDueToLoadEvent(SPGuiManager *src, SP_EVENT *event) {
    if ((*event) == EVENT_BACK) {
        slotWindowHide(src->loadGameWin);
        slotWindowDestroy(src->loadGameWin);
        src->loadGameWin = NULL;
        if (src->prevWin == MAIN_MENU_ACTIVE) {
            mainWindowShow(src->mainMenuWin);
            src->activeWin = MAIN_MENU_ACTIVE;
        } else {
            gameWindowShow(src->gameBoardWin);
            src->activeWin = GAME_WINDOW_ACTIVE;
        }

    } else if ((*event) == EVENT_LOAD_GAME) {
        SPGameState* gameState = loadGameFromSlot(src->gameSlotsManager, src->loadGameWin->chosenSlot - 1);
        if (gameState != NULL) {
            if (src->gameBoardWin != NULL) {
                gameWindowDestroy(src->gameBoardWin);
            }
            slotWindowHide(src->loadGameWin);
            slotWindowDestroy(src->loadGameWin);
            src->loadGameWin = NULL;
            src->gameBoardWin = gameWindowCreate(gameState);
            src->activeWin = GAME_WINDOW_ACTIVE;
        } else {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Chess", "Error: couldn't load savefile", NULL);
        }
    }
}

void handleManagerDueToSettingsEvent(SPGuiManager *src, SP_EVENT *event) {
    if ((*event) == EVENT_BACK) {
        settingWindowHide(src->settingsWin);
        mainWindowShow(src->mainMenuWin);
        src->activeWin = MAIN_MENU_ACTIVE;

    } else if ((*event) == EVENT_START_GAME) {
        SPSettings* settings = copySettings(src->settingsWin->gameSettings);
        SPGameState *gameState = createGameState(settings);
        settingWindowHide(src->settingsWin);
        settingsWindowDestroy(src->settingsWin);
        src->settingsWin = NULL;
        src->gameBoardWin = gameWindowCreate(gameState);
        src->activeWin = GAME_WINDOW_ACTIVE;
    }
}

void handleManagerDueToMainMenuEvent(SPGuiManager *src, SP_EVENT *event) {
    if ((*event) == EVENT_NEW_GAME) {
        mainWindowHide(src->mainMenuWin);
        src->activeWin = SETTINGS_WINDOW_ACTIVE;

    } else if ((*event) == EVENT_LOAD_MENU) {
        mainWindowHide(src->mainMenuWin);
        src->activeWin = LOAD_MENU_ACTIVE;
        src->prevWin = MAIN_MENU_ACTIVE;
    }
}
