//
// created by Ofir & Shawn on 11/09/2017.
//

#include "SPMainMenuWindow.h"

const SDL_Rect newGameR = {.x = 100, .y = 25, .h = 85, .w = 320};
const SDL_Rect loadR = {.x = 100, .y = 154, .h = 85, .w = 320};
const SDL_Rect exitR = {.x = 100, .y = 378, .h = 85, .w = 320};

SPMainMenuWin *mainWindowCreate() {
    SPMainMenuWin *res = (SPMainMenuWin *) calloc(sizeof(SPMainMenuWin), 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->window = createWindow(500, 500);
    if (res == NULL) {
        free(res);
        return NULL;
    }

    res->gameSlotsManager = getGameSlotsManagerInstance();

    res->newGameTexture = createTexture(res->window->renderer, NEW_GAME_BUTTON_PATH);
    if (res->newGameTexture == NULL) {
        mainWindowDestroy(res);
        return NULL;
    }

    res->loadTexture = createTexture(res->window->renderer, LOAD_BUTTON_BIG_PATH);
    if (res->loadTexture == NULL) {
        mainWindowDestroy(res);
        return NULL;
    }

    res->loadInactiveTexture = createTexture(res->window->renderer, LOAD_BUTTON_INACTIVE_BIG_PATH);
    if (res->loadInactiveTexture == NULL) {
        mainWindowDestroy(res);
        return NULL;
    }

    res->exitTexture = createTexture(res->window->renderer, EXIT_BUTTON_BIG_PATH);
    if (res->exitTexture == NULL) {
        mainWindowDestroy(res);
        return NULL;
    }
    return res;
}

void mainWindowDestroy(SPMainMenuWin *src) {
    if (!src) {
        return;
    }
    if (src->newGameTexture != NULL) {
        SDL_DestroyTexture(src->newGameTexture);
    }
    if (src->exitTexture != NULL) {
        SDL_DestroyTexture(src->exitTexture);
    }
    if (src->loadTexture != NULL) {
        SDL_DestroyTexture(src->loadTexture);
    }
    if (src->loadInactiveTexture != NULL) {
        SDL_DestroyTexture(src->loadInactiveTexture);
    }

    destroyWindow(src->window);
    free(src);
}

void mainWindowDraw(SPMainMenuWin *src) {
    if (src == NULL) {
        return;
    }
    SDL_SetRenderDrawColor(src->window->renderer, 238, 238, 210, 255);
    SDL_RenderClear(src->window->renderer);
    SDL_RenderCopy(src->window->renderer, src->newGameTexture, NULL, &newGameR);
    if (isLoadButtonAvailable(src)) {
        SDL_RenderCopy(src->window->renderer, src->loadTexture, NULL, &loadR);
    } else {
        SDL_RenderCopy(src->window->renderer, src->loadInactiveTexture, NULL, &loadR);
    }
    SDL_RenderCopy(src->window->renderer, src->exitTexture, NULL, &exitR);
    SDL_RenderPresent(src->window->renderer);
}

void mainWindowHide(SPMainMenuWin *src) {
    SDL_HideWindow(src->window->sdl_window);
}

void mainWindowShow(SPMainMenuWin *src) {
    SDL_ShowWindow(src->window->sdl_window);
}

bool isClickOnNewGame(int x, int y) {
    return isInRectangle(x, y, newGameR);
}

bool isLoadButtonAvailable(SPMainMenuWin* window) {
    return window->gameSlotsManager->numOfSlots > 0;
}

bool isClickOnLoad(int x, int y, SPMainMenuWin* window) {
    return isLoadButtonAvailable(window) && isInRectangle(x, y, loadR);
}

bool isClickOnExit(int x, int y) {
    return isInRectangle(x, y, exitR);
}

SP_EVENT mainWindowHandleEvent(SPMainMenuWin *win, SDL_Event *event) {
    if (!event) {
        return INVALID_EVENT;
    }
    switch (event->type) {
        case SDL_MOUSEBUTTONUP:
            if (isClickOnExit(event->button.x, event->button.y)) {
                return EVENT_QUIT;
            } else if (isClickOnNewGame(event->button.x, event->button.y)) {
                return EVENT_NEW_GAME;
            } else if (isClickOnLoad(event->button.x, event->button.y, win)) {
                return EVENT_LOAD_MENU;
            }
            break;

        case SDL_WINDOWEVENT:
            if (event->window.event == SDL_WINDOWEVENT_CLOSE) {
                return EVENT_QUIT;
            }
            break;

        default:
            return EVENT_NONE;
    }
    return EVENT_NONE;
}
