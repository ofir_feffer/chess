/*
 * SPGameWindow.c
 *
 *  Created on: Sep 12, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#include "SPGameBoardWindow.h"
#include "../core/SPMiniMax.h"

const SDL_Rect backgroundR = {.x = 0, .y = 0, .h = 720, .w = 1000};
const SDL_Rect loadR_3 = {.x = 59, .y = 243, .h = 43, .w = 160};
const SDL_Rect undoR = {.x = 59, .y = 333, .h = 43, .w = 160};
const SDL_Rect restartR = {.x = 59, .y = 63, .h = 43, .w = 160};
const SDL_Rect saveR = {.x = 59, .y = 153, .h = 43, .w = 160};
const SDL_Rect mainMenuR = {.x = 59, .y = 500, .h = 43, .w = 160};
const SDL_Rect exitR_2 = {.x = 59, .y = 590, .h = 43, .w = 160};

SPGameBoardWin *gameWindowCreate(SPGameState *gameState) {
    SPGameBoardWin *res = (SPGameBoardWin *) calloc(sizeof(SPGameBoardWin), 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->window = createWindow(1000, 720);
    if (res->window == NULL) {
        free(res);
        return NULL;
    }

    res->gameState = gameState;
    res->gameSlotsManager = getGameSlotsManagerInstance();
    res->isGameSaved = false;

    res->backgroundTexture = createTexture(res->window->renderer, GAME_BOARD_BACKGROUND_PATH);
    if (res->backgroundTexture == NULL) {
        gameWindowDestroy(res);
        return NULL;
    }

    bool ok = createPiecesTextures(res);
    if (!ok) {
        gameWindowDestroy(res);
        return NULL;
    }

    ok = createGameWindowButtons(res);
    if (!ok) {
        gameWindowDestroy(res);
        return NULL;
    }

    return res;
}

bool createPiecesTextures(SPGameBoardWin *win) {
    win->blackBishopTexture = createTexture(win->window->renderer, BLACK_BISHOP_PATH);
    if (win->blackBishopTexture == NULL) {
        return false;
    }

    win->blackKingTexture = createTexture(win->window->renderer, BLACK_KING_PATH);
    if (win->blackKingTexture == NULL) {
        return false;
    }

    win->blackKnightTexture = createTexture(win->window->renderer, BLACK_KNIGHT_PATH);
    if (win->blackKnightTexture == NULL) {
        return false;
    }

    win->blackPawnTexture = createTexture(win->window->renderer, BLACK_PAWN_PATH);
    if (win->blackPawnTexture == NULL) {
        return false;
    }
    win->blackQueenTexture = createTexture(win->window->renderer, BLACK_QUEEN_PATH);
    if (win->blackQueenTexture == NULL) {
        return false;
    }
    win->blackRookTexture = createTexture(win->window->renderer, BLACK_ROOK_PATH);
    if (win->blackRookTexture == NULL) {
        return false;
    }

    win->whiteBishopTexture = createTexture(win->window->renderer, WHITE_BISHOP_PATH);
    if (win->whiteBishopTexture == NULL) {
        return false;
    }

    win->whiteKingTexture = createTexture(win->window->renderer, WHITE_KING_PATH);
    if (win->whiteKingTexture == NULL) {
        return false;
    }

    win->whiteKnightTexture = createTexture(win->window->renderer, WHITE_KNIGHT_PATH);
    if (win->whiteKnightTexture == NULL) {
        return false;
    }

    win->whitePawnTexture = createTexture(win->window->renderer, WHITE_PAWN_PATH);
    if (win->whitePawnTexture == NULL) {
        return false;
    }
    win->whiteQueenTexture = createTexture(win->window->renderer, WHITE_QUEEN_PATH);
    if (win->whiteQueenTexture == NULL) {
        return false;
    }

    win->whiteRookTexture = createTexture(win->window->renderer, WHITE_ROOK_PATH);
    if (win->whiteRookTexture == NULL) {
        return false;
    }
    return true;
}

bool createGameWindowButtons(SPGameBoardWin *win) {
    win->exitTexture = createTexture(win->window->renderer, EXIT_BUTTON_SMALL_PATH);
    if (win->exitTexture == NULL) {
        return false;
    }

    win->loadTexture = createTexture(win->window->renderer, LOAD_BUTTON_SMALL_PATH);
    if (win->loadTexture == NULL) {
        return false;
    }

    win->loadInactiveTexture = createTexture(win->window->renderer, LOAD_BUTTON_INACTIVE_SMALL_PATH);
    if (win->loadInactiveTexture == NULL) {
        return false;
    }

    win->undoTexture = createTexture(win->window->renderer, UNDO_BUTTON_PATH);
    if (win->undoTexture == NULL) {
        return false;
    }

    win->undoInactiveTexture = createTexture(win->window->renderer, UNDO_BUTTON_INACTIVE_PATH);
    if (win->undoInactiveTexture == NULL) {
        return false;
    }

    win->mainMenuTexture = createTexture(win->window->renderer, MAIN_MENU_BUTTON_PATH);
    if (win->mainMenuTexture == NULL) {
        return false;
    }

    win->restartTexture = createTexture(win->window->renderer, RESTART_BUTTON_PATH);
    if (win->restartTexture == NULL) {
        return false;
    }

    win->saveTexture = createTexture(win->window->renderer, SAVE_BUTTON_PATH);
    if (win->saveTexture == NULL) {
        return false;
    }

    win->saveInactiveTexture = createTexture(win->window->renderer, SAVE_BUTTON_INACTIVE_PATH);
    if (win->saveInactiveTexture == NULL) {
        return false;
    }
    return true;
}

void gameWindowDraw(SPGameBoardWin *src) {
    if (src == NULL) {
        return;
    }
    SDL_SetRenderDrawColor(src->window->renderer, 255, 255, 255, 255);
    SDL_RenderClear(src->window->renderer);
    SDL_RenderCopy(src->window->renderer, src->backgroundTexture, NULL, &backgroundR);
    SDL_RenderCopy(src->window->renderer, src->restartTexture, NULL, &restartR);
    if (isSaveButtonAvailable(src)) {
        SDL_RenderCopy(src->window->renderer, src->saveTexture, NULL, &saveR);
    } else {
        SDL_RenderCopy(src->window->renderer, src->saveInactiveTexture, NULL, &saveR);
    }
    if (isUndoButtonAvailable(src)) {
        SDL_RenderCopy(src->window->renderer, src->undoTexture, NULL, &undoR);
    } else {
        SDL_RenderCopy(src->window->renderer, src->undoInactiveTexture, NULL, &undoR);
    }
    if (gameWindowIsLoadButtonAvailable(src)) {
        SDL_RenderCopy(src->window->renderer, src->loadTexture, NULL, &loadR_3);
    } else {
        SDL_RenderCopy(src->window->renderer, src->loadInactiveTexture, NULL, &loadR_3);
    }
    SDL_RenderCopy(src->window->renderer, src->mainMenuTexture, NULL, &mainMenuR);
    SDL_RenderCopy(src->window->renderer, src->exitTexture, NULL, &exitR_2);

    SDL_Texture *pieceTexture;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (src->dragAndDropStarted && i == src->draggedPieceRow && j == src->draggedPieceColumn) {
                continue; // we will draw it later so it will be on top
            }
            pieceTexture = pieceToTexture(src, src->gameState->board[i][j]);
            if (pieceTexture != NULL) {
                SDL_Rect rect = {.x = 280 + 90 * j, .y = 630 - 90 * i, .h = 91, .w = 91};
                SDL_RenderCopy(src->window->renderer, pieceTexture, NULL, &rect);
            }
        }
    }

    if (src->dragAndDropStarted) { // drawing the dragged piece last so it will be on top
        if (src->cursorLocationX < 250) {
            src->cursorLocationX = 250; //making sure the piece isn't drawn outside board limits
        }
        pieceTexture = pieceToTexture(src, src->gameState->board[src->draggedPieceRow][src->draggedPieceColumn]);
        SDL_Rect rect = {.x = src->cursorLocationX, .y = src->cursorLocationY, .h = 91, .w = 91};
        SDL_RenderCopy(src->window->renderer, pieceTexture, NULL, &rect);
    }

    SDL_RenderPresent(src->window->renderer);
}

void gameWindowDestroy(SPGameBoardWin *src) {
    if (src == NULL)
        return;
    if (src->blackBishopTexture != NULL) {
        SDL_DestroyTexture(src->blackBishopTexture);
    }
    if (src->blackKingTexture != NULL) {
        SDL_DestroyTexture(src->blackKingTexture);
    }
    if (src->blackKnightTexture != NULL) {
        SDL_DestroyTexture(src->blackKnightTexture);
    }
    if (src->blackPawnTexture != NULL) {
        SDL_DestroyTexture(src->blackPawnTexture);
    }
    if (src->blackQueenTexture != NULL) {
        SDL_DestroyTexture(src->blackQueenTexture);
    }
    if (src->blackRookTexture != NULL) {
        SDL_DestroyTexture(src->blackRookTexture);
    }
    if (src->whiteBishopTexture != NULL) {
        SDL_DestroyTexture(src->whiteBishopTexture);
    }
    if (src->whiteKingTexture != NULL) {
        SDL_DestroyTexture(src->whiteKingTexture);
    }
    if (src->whiteKnightTexture != NULL) {
        SDL_DestroyTexture(src->whiteKnightTexture);
    }
    if (src->whitePawnTexture != NULL) {
        SDL_DestroyTexture(src->whitePawnTexture);
    }
    if (src->whiteQueenTexture != NULL) {
        SDL_DestroyTexture(src->whiteQueenTexture);
    }
    if (src->whiteRookTexture != NULL) {
        SDL_DestroyTexture(src->whiteRookTexture);
    }
    if (src->exitTexture != NULL) {
        SDL_DestroyTexture(src->exitTexture);
    }
    if (src->loadTexture != NULL) {
        SDL_DestroyTexture(src->loadTexture);
    }
    if (src->loadInactiveTexture != NULL) {
        SDL_DestroyTexture(src->loadInactiveTexture);
    }
    if (src->mainMenuTexture != NULL) {
        SDL_DestroyTexture(src->mainMenuTexture);
    }
    if (src->restartTexture != NULL) {
        SDL_DestroyTexture(src->restartTexture);
    }
    if (src->saveTexture != NULL) {
        SDL_DestroyTexture(src->saveTexture);
    }
    if (src->saveInactiveTexture != NULL) {
        SDL_DestroyTexture(src->saveInactiveTexture);
    }
    if (src->undoTexture != NULL) {
        SDL_DestroyTexture(src->undoTexture);
    }
    if (src->undoInactiveTexture != NULL) {
        SDL_DestroyTexture(src->undoInactiveTexture);
    }
    if (src->backgroundTexture != NULL) {
        SDL_DestroyTexture(src->backgroundTexture);
    }
    destroyGameState(src->gameState);
    destroyWindow(src->window);
    free(src);
}

void gameWindowHide(SPGameBoardWin *src) {
    SDL_HideWindow(src->window->sdl_window);
}

void gameWindowShow(SPGameBoardWin *src) {
    SDL_ShowWindow(src->window->sdl_window);
}

bool isClickOnRestart(int x, int y) {
    return isInRectangle(x, y, restartR);
}

bool gameWindowIsClickOnExit(int x, int y) {
    return isInRectangle(x, y, exitR_2);
}

bool isSaveButtonAvailable(SPGameBoardWin *window) {
    return !window->isGameOver && !window->isGameSaved;
}

bool isClickOnSave(int x, int y, SPGameBoardWin *window) {
    return isSaveButtonAvailable(window) && isInRectangle(x, y, saveR);
}

bool isUndoButtonAvailable(SPGameBoardWin *window) {
    return !window->isGameOver && window->gameState->settings->game_mode == 1 &&
           window->gameState->history->actualSize > 1;
}

bool isClickOnUndo(int x, int y, SPGameBoardWin *window) {
    return isUndoButtonAvailable(window) && isInRectangle(x, y, undoR);
}

bool isClickOnMainMenu(int x, int y) {
    return isInRectangle(x, y, mainMenuR);
}

bool gameWindowIsLoadButtonAvailable(SPGameBoardWin *win) {
    return win->gameSlotsManager != NULL && win->gameSlotsManager->numOfSlots > 0;
}

bool gameWindowIsClickOnLoad(int x, int y, SPGameBoardWin *win) {
    return gameWindowIsLoadButtonAvailable(win) && isInRectangle(x, y, loadR_3);
}

bool handleDragAndDropMouseDown(SPGameBoardWin *window, SDL_Event *event) {
    window->draggedPieceRow = (event->button.y - 90 - 630) / (-90);
    window->draggedPieceColumn = (event->button.x - 280) / 90;
    window->cursorLocationX = event->button.x - 45;
    window->cursorLocationY = event->button.y - 45;
    char piece = window->gameState->board[window->draggedPieceRow][window->draggedPieceColumn];
    if (isIndexInBounds(window->draggedPieceRow, window->draggedPieceColumn) &&
        isPlayerPiece(window->gameState->currentPlayer, piece)) {
        window->dragAndDropStarted = true;
        return true;
    }
    return false;
}

void handleDragAndDropMouseUp(SPGameBoardWin *window, SDL_Event *event) {
    window->dragAndDropStarted = false;
    int z = (event->button.y - 90 - 630) / (-90);
    int w = (event->button.x - 280) / 90;
    if (z == window->draggedPieceRow && w == window->draggedPieceColumn) {
        return;
    }
    SPMoveResult result = makeMove(window->gameState, window->draggedPieceRow, window->draggedPieceColumn, z, w);
    if (!result.success || result.checkmate || result.tie || result.check) {
        gameWindowDraw(window);
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Message", result.msg, NULL);
    }
    if (result.checkmate || result.tie) {
        window->isGameOver = true;
    }
    if (result.success) {
        window->isGameSaved = false;
    }
}

SP_EVENT areYouSure(SPGameBoardWin *window, SP_EVENT event) {
    SDL_MessageBoxButtonData yesButton = {.flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, .buttonid = 0, .text = "Yes"};
    SDL_MessageBoxButtonData noButton = {.flags = 0, .buttonid = 1, .text = "No"};
    SDL_MessageBoxButtonData cancelButton = {.flags = SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, .buttonid = 2, .text = "Cancel"};
    SDL_MessageBoxButtonData buttons[3] = {cancelButton, noButton, yesButton};

    SDL_MessageBoxData messageBoxData = {.flags = SDL_MESSAGEBOX_WARNING, .window = NULL, .title = "Chess",
            .message = "Save current game before closing?", .numbuttons = 3, .buttons = buttons, .colorScheme = NULL};
    int res;
    SDL_ShowMessageBox(&messageBoxData, &res);

    if (res == 2) { // case: cancel
        return EVENT_NONE;
    } else if (res == 0) { // case: yes
        gameWindowSaveGame(window);
    }

    return event;
}

void gameWindowSaveGame(SPGameBoardWin *window) {
    bool ok = saveGameToFirstSlot(window->gameSlotsManager, window->gameState);
    if (ok) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Chess", "Game was saved successfully", NULL);
        window->isGameSaved = true;
    } else {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Chess", "Error: game could not be saved", NULL);
    }
}

void gameWindowRestartGame(SPGameBoardWin *window) {
    setupBoard(window->gameState);
    window->gameState->currentPlayer = 1;
    spArrayListClear(window->gameState->history);
    window->isGameOver = false;
    window->isGameSaved = false;
}

void gameWindowUndoMove(SPGameBoardWin *window) {
    undoMove(window->gameState, false);
    undoMove(window->gameState, false);
    window->isGameSaved = false;
}

SP_EVENT gameWindowHandleEvent(SPGameBoardWin *window, SDL_Event *event) {
    if (!event) {
        return INVALID_EVENT;
    }
    switch (event->type) {
        case SDL_MOUSEBUTTONDOWN:
            if (!window->isGameOver && handleDragAndDropMouseDown(window, event)) {
                return EVENT_DRAG_AND_DROP;
            }
            break;

        case SDL_MOUSEMOTION:
            if (window->dragAndDropStarted) {
                window->cursorLocationX = event->button.x - 45;
                window->cursorLocationY = event->button.y - 45;
                return EVENT_DRAG_AND_DROP;
            }
            break;

        case SDL_MOUSEBUTTONUP:
            return gameWindowHandleMouseButtonUpEvent(window, event);

        case SDL_WINDOWEVENT:
            if (event->window.event == SDL_WINDOWEVENT_CLOSE) {
                return EVENT_QUIT;
            }
            break;

        default:
            return EVENT_NONE;
    }
    return EVENT_NONE;
}

SP_EVENT gameWindowHandleMouseButtonUpEvent(SPGameBoardWin *window, SDL_Event *event) {
    if (window->dragAndDropStarted) {
        handleDragAndDropMouseUp(window, event);
        return EVENT_DRAG_AND_DROP;

    } else if (gameWindowIsClickOnExit(event->button.x, event->button.y)) {
        if (isSaveButtonAvailable(window)) {
            return areYouSure(window, EVENT_QUIT);
        }
        return EVENT_QUIT;

    } else if (isClickOnMainMenu(event->button.x, event->button.y)) {
        if (isSaveButtonAvailable(window)) {
            return areYouSure(window, EVENT_MAIN_MENU);
        }
        return EVENT_MAIN_MENU;

    } else if (isClickOnSave(event->button.x, event->button.y, window)) {
        gameWindowSaveGame(window);
        return EVENT_SAVE_GAME;

    } else if (gameWindowIsClickOnLoad(event->button.x, event->button.y, window)) {
        return EVENT_LOAD_MENU;

    } else if (isClickOnRestart(event->button.x, event->button.y)) {
        gameWindowRestartGame(window);
        return EVENT_RESTART;

    } else if (isClickOnUndo(event->button.x, event->button.y, window)) {
        gameWindowUndoMove(window);
        return EVENT_UNDO;

    } else {
        return EVENT_NONE;
    }
}

SDL_Texture *pieceToTexture(SPGameBoardWin *window, char piece) {
    switch (piece) {
        case WHITE_PAWN:
            return window->whitePawnTexture;
        case WHITE_BISHOP:
            return window->whiteBishopTexture;
        case WHITE_ROOK:
            return window->whiteRookTexture;
        case WHITE_KNIGHT:
            return window->whiteKnightTexture;
        case WHITE_QUEEN:
            return window->whiteQueenTexture;
        case WHITE_KING:
            return window->whiteKingTexture;
        case BLACK_PAWN:
            return window->blackPawnTexture;
        case BLACK_BISHOP:
            return window->blackBishopTexture;
        case BLACK_ROOK:
            return window->blackRookTexture;
        case BLACK_KNIGHT:
            return window->blackKnightTexture;
        case BLACK_QUEEN:
            return window->blackQueenTexture;
        case BLACK_KING:
            return window->blackKingTexture;
        default:
            return NULL;
    }
}


