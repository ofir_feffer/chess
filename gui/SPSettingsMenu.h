//
// created by Ofir & Shawn on 11/09/2017.
//

#ifndef CHESS_SPSETTINGSMENU_H
#define CHESS_SPSETTINGSMENU_H

#include <stdbool.h>
#include <stdio.h>
#include "SPWindow.h"
#include "../core/SPSettings.h"
#include "../guiUtil/SPGuiUtil.h"
#include "../guiUtil/SPFilsPaths.h"

typedef struct settingsWin {
    SPSettings *gameSettings;
    SPWindow* window;
    SDL_Texture *onePlayerTexture;
    SDL_Texture *onePlayerInactiveTexture;
    SDL_Texture *twoPlayerTexture;
    SDL_Texture *twoPlayerInactiveTexture;
    SDL_Texture *noobTexture;
    SDL_Texture *noobInactiveTexture;
    SDL_Texture *easyTexture;
    SDL_Texture *easyInactiveTexture;
    SDL_Texture *moderateTexture;
    SDL_Texture *moderateInactiveTexture;
    SDL_Texture *hardTexture;
    SDL_Texture *hardInactiveTexture;
    SDL_Texture *whiteTexture;
    SDL_Texture *whiteInactiveTexture;
    SDL_Texture *blackTexture;
    SDL_Texture *blackInactiveTexture;
    SDL_Texture *startTexture;
    SDL_Texture *backTexture;
    SDL_Texture *gameModeLabelTexture;
    SDL_Texture *difficultyLabelTexture;
    SDL_Texture *colorLabelTexture;
} SPSettingsWin;


/**
 * creates a game settings window and initializes all textures needed
 * @return
 * SPSettingsWin instance.
 */
SPSettingsWin *settingsWindowCreate();

/**
 * gets a game settings window that is in its initialization process,
 * and loads all textures of text labels.
 * @param win - game settings window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createLabelsTextures(SPSettingsWin* win);

/**
 * gets a game settings window that is in its initialization process,
 * and loads all textures of possible game modes
 * @param win - game settings window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createGameModesButtons(SPSettingsWin* win);

/**
 * gets a game settings window that is in its initialization process,
 * and loads all textures of possible difficulties
 * @param win - game settings window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createDifficultiesButtons(SPSettingsWin* win);

/**
 * gets a game settings window that is in its initialization process,
 * and loads all textures of possible user colors
 * @param win - game settings window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createUserColorTextures(SPSettingsWin* win);

/**
 * draws a game settings window according to its state (the chosen settings)
 * @param src - game settings window instance
 */
void settingsWindowDraw(SPSettingsWin *src);

/**
 * Frees all memory resources associated with a game settings window instance.
 * if the source window is NULL then the function does nothing
 * @param src - game settings window instance
 */
void settingsWindowDestroy(SPSettingsWin *src);

/**
 * hides a game settings window
 * @param src - @NotNULL game settings window instance
 */
void settingWindowHide(SPSettingsWin *src);

/**
 * shows a game settings window
 * @param src - @NotNULL game settings window instance
 */
void settingsWindowShow(SPSettingsWin *src);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the 'One player' button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'One player' button, false otherwise
 */
bool isClickOnOnePlayer(int x, int y);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the 'Two players' button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'Two players' button, false otherwise
 */
bool isClickOnTwoPlayers(int x, int y);

/**
 * gets coordinates of a mouse click and returns true if the click is on the noob button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'Noob' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnNoob(int x, int y, SPSettingsWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the 'Easy' button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'Easy' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnEasy(int x, int y, SPSettingsWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the 'Moderate' button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'Moderate' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnModerate(int x, int y, SPSettingsWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the 'Hard' button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'Hard' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnHard(int x, int y, SPSettingsWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the 'Black' button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'Black' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnBlack(int x, int y, SPSettingsWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the 'White' button AND game mode is set to 1 player
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param window - @NotNULL game settings window instance
 * @return
 * true if (x,y) is in the rectangle of the 'White' button AND game mode is set to 1 player, false otherwise
 */
bool isClickOnWhite(int x, int y, SPSettingsWin* window);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the 'Back' button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'Back' button, false otherwise
 */
bool isClickOnBack(int x, int y);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the 'Start' button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'Start' button, false otherwise
 */
bool isClickOnStart(int x, int y);

/**
 * gets a game settings window instance and a SDL_Event.
 * changes the window state according to the event's type and parameters
 * @param window - @NotNULL game settings window instance
 * @param event - a window or mouse event
 * @return
 * SP_EVENT relevant to the change to the window's state:
 *      EVENT_QUIT if user clicked on exit button
 *      EVENT_CHANGED_SETTINGS if user clicked on a setting option
 *      EVENT_BACK if user clicked on the 'Back' button
 *      EVENT_START_GAME if user clicked on the 'Start' button
 *      EVENT_NONE otherwise
 */
SP_EVENT settingsWindowHandleEvent(SPSettingsWin *window, SDL_Event *event);


#endif
