/*
 * SPGameSlots.h
 *
 *  Created on: Sep 12, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#ifndef SPGAMESLOTS_H_
#define SPGAMESLOTS_H_

#include <stdbool.h>
#include <SDL.h>
#include <stdio.h>
#include "SPWindow.h"
#include "../guiUtil/SPGuiUtil.h"
#include "../guiUtil/SPGameSlotsManager.h"
#include "../guiUtil/SPFilsPaths.h"

typedef struct {
    SPWindow* window;
    SDL_Texture *slotsTextures[5];
    SDL_Texture *slotsInactiveTextures[5];
    SDL_Texture *loadTexture;
    SDL_Texture *loadInactiveTexture;
    SDL_Texture *backTexture;
    int chosenSlot;
    SPGameSlotsManager *slotsManager;
} SPGameSlotsWin;

/**
 * creates a slots window and initializes all textures needed
 * @return
 * SPGameSlotsWin instance.
 */
SPGameSlotsWin *slotWindowCreate();

/**
 * gets a slots window that is in its initialization process,
 * and loads all textures of slots' buttons
 * @param win - slots window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createSlotButtons(SPGameSlotsWin* win);

/**
 * draws a slots window according to its state (the pressed slot)
 * @param src - slots window instance
 */
void slotWindowDraw(SPGameSlotsWin *src);

/**
 * Frees all memory resources associated with a slots window instance.
 * if the source window is NULL then the function does nothing
 * @param src - slots window instance
 */
void slotWindowDestroy(SPGameSlotsWin *src);

/**
 * hides a slots window
 * @param src - @NotNULL slots window instance
 */
void slotWindowHide(SPGameSlotsWin *src);

/**
 * shows a slots window
 * @param src - @NotNULL slots window instance
 */
void slotWindowShow(SPGameSlotsWin *src);

/**
 * gets an index in the range 0-4, and two integers indicating a point where the mouse was clicked.
 * returns true if the mouse click is on the button of the slot represented by the index
 * @param slotNumber - integer in range 0-4
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of 'slotNumber' button, false otherwise
 */
bool isClickOnSlot(int slotNumber, int x, int y);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the back button
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'back' button, false otherwise
 */
bool spGameSlotsIsClickOnBack(int x, int y);

/**
 * gets two integers indicating a point where the mouse was clicked,
 * returns true if the mouse click is on the load button AND the load button is active
 * @param x - x-axis value of a mouse click location
 * @param y - y-axis value of a mouse click location
 * @return
 * true if point (x,y) is in the rectangle of the 'load' button AND the load button is active, false otherwise
 */
bool spGameSlotsIsClickOnLoad(int x, int y, SPGameSlotsWin* window);

/**
 * gets a slots window instance and a SDL_Event.
 * changes the window state according to the event's type and parameters
 * @param window - @NotNULL slots window instance
 * @param event - a window or mouse event
 * @return
 * SP_EVENT relevant to the change to the window's state
 */
SP_EVENT slotWindowHandleEvent(SPGameSlotsWin *src, SDL_Event *event);


#endif //CHESS_SPMAINMENU_H
