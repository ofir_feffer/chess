/*
 * SPGameWindow.h
 *
 *  Created on: Sep 12, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#ifndef SPGAMEWINDOW_H_
#define SPGAMEWINDOW_H_

#include <stdbool.h>
#include <SDL.h>
#include <stdio.h>
#include "SPWindow.h"
#include "../guiUtil/SPGuiUtil.h"
#include "../util/SPUtil.h"
#include "../core/SPGameActions.h"
#include "../guiUtil/SPFilsPaths.h"
#include "../guiUtil/SPGameSlotsManager.h"

typedef struct {
    SPWindow* window;
    SPGameState *gameState;
    SPGameSlotsManager* gameSlotsManager;
    SDL_Texture *backgroundTexture;
    SDL_Texture *restartTexture;
    SDL_Texture *saveTexture;
    SDL_Texture *saveInactiveTexture;
    SDL_Texture *loadTexture;
    SDL_Texture *loadInactiveTexture;
    SDL_Texture *undoTexture;
    SDL_Texture *undoInactiveTexture;
    SDL_Texture *exitTexture;
    SDL_Texture *mainMenuTexture;
    SDL_Texture *blackPawnTexture;
    SDL_Texture *blackKnightTexture;
    SDL_Texture *blackBishopTexture;
    SDL_Texture *blackKingTexture;
    SDL_Texture *blackQueenTexture;
    SDL_Texture *blackRookTexture;
    SDL_Texture *whitePawnTexture;
    SDL_Texture *whiteKnightTexture;
    SDL_Texture *whiteBishopTexture;
    SDL_Texture *whiteKingTexture;
    SDL_Texture *whiteQueenTexture;
    SDL_Texture *whiteRookTexture;
    bool isGameOver;
    bool isGameSaved;

    // variables for drag and drop
    bool dragAndDropStarted;
    int draggedPieceRow;
    int draggedPieceColumn;
    int cursorLocationX; // current location of cursor
    int cursorLocationY; // current location of cursor
} SPGameBoardWin;

/**
 * creates a game board window and initializes all textures needed (chess pieces, board, and buttons)
 * @param gameState - a game state instance
 * @return
 * SPGameBoardWin instance.
 */
SPGameBoardWin *gameWindowCreate(SPGameState *gameState);

/**
 * gets a game board window that is in its initialization process,
 * and loads all textures of chess pieces (rook, bishop, etc...)
 * @param win - game board window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createPiecesTextures(SPGameBoardWin *win);

/**
 * gets a game board window that is in its initialization process,
 * and loads all textures of the window's buttons
 * @param win - game board window instance
 * @return
 * true if all textures were loaded successfully, false otherwise
 */
bool createGameWindowButtons(SPGameBoardWin *win);

/**
 * gets a game board window instance and draws it according to the current game state
 * @param src - game board window instance
 */
void gameWindowDraw(SPGameBoardWin *src);

/**
 * Frees all memory resources associated with a game board window instance.
 * if the source window is NULL then the function does nothing
 * @param src - game board window instance
 */
void gameWindowDestroy(SPGameBoardWin *src);

/**
 * hides a game board window
 * @param src - @NotNULL game board instance
 */
void gameWindowHide(SPGameBoardWin *src);

/**
 * shows a game board window
 * @param src - @NotNULL game board instance
 */
void gameWindowShow(SPGameBoardWin *src);

/**
 * gets coordinates of a mouse click and returns true if the click is on the restart button.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @return
 * true if (x,y) is in the rectangle of the restart button, false otherwise
 */
bool isClickOnRestart(int x, int y);

/**
 * gets coordinates of a mouse click and returns true if the click is on the exit button.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @return
 * true if (x,y) is in the rectangle of the exit button, false otherwise
 */
bool gameWindowIsClickOnExit(int x, int y);

/**
 * gets a game board window instance and returns true if there are any saved game slots to load
 * @param win - @NotNULL game board window instance
 * @return
 * true if there are any saved game slots to load, false otherwise
 */
bool gameWindowIsLoadButtonAvailable(SPGameBoardWin *win);

/**
 * gets coordinates of a mouse click and returns true if the click is on the load button AND load button is active.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param win - @NotNULL game board window instance
 * @return
 * true if (x,y) is in the rectangle of the load button AND load button is active, false otherwise
 */
bool gameWindowIsClickOnLoad(int x, int y, SPGameBoardWin *win);

/**
 * gets a game board window instance and returns true if the game is not over AND game is not saved
 * @param window - @NotNULL game board window instance
 * @return
 * true if the game is not over AND game is not saved, false otherwise
 */
bool isSaveButtonAvailable(SPGameBoardWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the save button AND save button is active.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param win - @NotNULL game board window instance
 * @return
 * true if (x,y) is in the rectangle of the save button AND save button is active, false otherwise
 */
bool isClickOnSave(int x, int y, SPGameBoardWin* window);

/**
 * gets a game board window instance and returns true
 * if the game is not over AND game mode is 1-player AND moves history is not empty
 * @param window - @NotNULL game board window instance
 * @return
 * true if the game is not over AND game mode is 1-player AND moves' history is not empty, false otherwise
 */
bool isUndoButtonAvailable(SPGameBoardWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the undo button AND undo button is active.
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param win - @NotNULL game board window instance
 * @return
 * true if (x,y) is in the rectangle of the undo button AND undo button is active, false otherwise
 */
bool isClickOnUndo(int x, int y, SPGameBoardWin* window);

/**
 * gets coordinates of a mouse click and returns true if the click is on the main menu button
 * @param x - x-axis value of the mouse click
 * @param y - y-axis value of the mouse click
 * @param win - @NotNULL game board window instance
 * @return
 * true if (x,y) is in the rectangle of the main menu button, false otherwise
 */
bool isClickOnMainMenu(int x, int y);

/**
 * gets a game board window instance and an SDL_Event of a mouse button down.
 * if the mouse click is on a current player's piece then the function sets the dragAndDropStarted flag to true
 * @param window - @NotNULL game board window instance
 * @param event - SDL_Event of a mouse button down
 * @return
 * true if the mouse click is on a current player's piece, false otherwise
 */
bool handleDragAndDropMouseDown(SPGameBoardWin *window, SDL_Event *event);

/**
 * gets a game board window and an SDL_Event of a mouse button up.
 * retrieves the coordinates of the cell that the mouse button was released on,
 * and tries to make the move that the user requested.
 * function should be called ONLY if the window's dragAndDropStarted flag is set to true.
 * @param window - @NotNULL game board window instance
 * @param event - SDL_Event of a mouse button up
 */
void handleDragAndDropMouseUp(SPGameBoardWin *window, SDL_Event *event);

/**
 * gets a game board window instance and a SP_Event.
 * the function creates a message box asking the user if he wants to save the game.
 * if the user hits the 'cancel' button then the function will return EVENT_NONE
 * if the user hits the 'no' button then the function will return the event that was given to it as a parameter
 * if the user hits the 'yes' button then the function will save the game and return the event that was given to it as a parameter
 * @param window - @NotNULL game board window instance
 * @param event - event to return in case the user hits the 'yes' or 'no' button
 * @return
 * EVENT_NONE if the user hit the 'cancel' button, the parameter event otherwise
 */
SP_EVENT areYouSure(SPGameBoardWin* window, SP_EVENT event);

/**
 * gets a game board window instance and saves the game.
 * shows a message box with the result (success or failure)
 * @param window - @NotNULL game board window instance
 */
void gameWindowSaveGame(SPGameBoardWin *window);

/**
 * gets a game board window instance and resets the game to the starting state,
 * by moving all chess pieces back to their initial cells, clearing the moves' history and setting the current player to be the white player
 * @param window - @NotNULL game board window instance
 */
void gameWindowRestartGame(SPGameBoardWin *window);

/**
 * gets a game board window instance and undo the last 2 moves.
 * function should be called only if game mode is set to 1-player and history is not empty
 * @param window - @NotNULL game board window instance
 */
void gameWindowUndoMove(SPGameBoardWin *window);

/**
 * gets a game board window instance and a SDL_Event.
 * changes the window state according to the event's type and parameters
 * @param window - @NotNULL game board window instance
 * @param event - a window or mouse event
 * @return
 * SP_EVENT relevant to the change to the window's state
 */
SP_EVENT gameWindowHandleEvent(SPGameBoardWin *window, SDL_Event *event);

/**
 * gets a game board window instance and a SDL_Event of a mouse button up.
 * changes the window state according to the click's location.
 * @param window - @NotNULL game board window instance
 * @param event - a  mouse button up event
 * @return
 * SP_EVENT relevant to the change to the window's state
 */
SP_EVENT gameWindowHandleMouseButtonUpEvent(SPGameBoardWin *window, SDL_Event *event);

/**
 * gets a game board window instance, and a character representing a chess piece.
 * returns the texture of the chess piece that is represented by the character.
 * @param window - @NotNULL game board window instance
 * @param piece - a character representing a chess piece
 * @return
 * NULL if character doesn't represent any chess piece, the texture of the chess piece that is represented by the character otherwise
 */
SDL_Texture *pieceToTexture(SPGameBoardWin *window, char piece);


#endif /* SPGAMEWINDOW_H_ */
