//
// created by Ofir & Shawn on 21/09/2017.
//

#include "SPWindow.h"

SPWindow* createWindow(int width, int height) {
    SPWindow* res = malloc(sizeof(SPWindow));
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->sdl_window = SDL_CreateWindow(DEFAULT_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
                                       SDL_WINDOW_OPENGL);
    if (res->sdl_window == NULL) {
        printf(ERROR_WINDOW_CREATION, SDL_GetError());
        return NULL;
    }

    res->renderer = SDL_CreateRenderer(res->sdl_window, -1, SDL_RENDERER_ACCELERATED);
    if (res->renderer == NULL) {
        SDL_DestroyWindow(res->sdl_window);
        printf(ERROR_WINDOW_CREATION, SDL_GetError());
        return NULL;
    }

    return res;
}

void destroyWindow(SPWindow* src) {
    if (src == NULL) {
        return;
    }
    if (src->renderer != NULL) {
        SDL_DestroyRenderer(src->renderer);
    }
    if (src->sdl_window != NULL) {
        SDL_DestroyWindow(src->sdl_window);
    }
    free(src);
}