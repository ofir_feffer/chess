/*
 * SPGameSlots.c
 *
 *  Created on: Sep 12, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#include "SPGameSlotsWindow.h"


const SDL_Rect slotsRects[5] = {
        {.x = 207, .y = 20, .h = 43, .w = 160},
        {.x = 207, .y = 107, .h = 43, .w = 160},
        {.x = 207, .y = 194, .h = 43, .w = 160},
        {.x = 207, .y = 281, .h = 43, .w = 160},
        {.x = 207, .y = 368, .h = 43, .w = 160},
};
const SDL_Rect backR_2 = {.x = 40, .y = 486, .h = 43, .w = 160};
const SDL_Rect loadR_2 = {.x = 375, .y = 486, .h = 43, .w = 160};

SPGameSlotsWin *slotWindowCreate() {
    SPGameSlotsWin *res = (SPGameSlotsWin *) calloc(sizeof(SPGameSlotsWin), 1);
    if (res == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }

    res->window = createWindow(550, 550);
    if (res->window == NULL) {
        free(res);
        return NULL;
    }

    res->chosenSlot = 0;
    res->slotsManager = getGameSlotsManagerInstance();

    bool ok = createSlotButtons(res);
    if (!ok) {
        slotWindowDestroy(res);
        return NULL;
    }

    res->loadTexture = createTexture(res->window->renderer, LOAD_BUTTON_SMALL_PATH);
    if (res->loadTexture == NULL) {
        slotWindowDestroy(res);
        return NULL;
    }

    res->loadInactiveTexture = createTexture(res->window->renderer, LOAD_BUTTON_INACTIVE_SMALL_PATH);
    if (res->loadTexture == NULL) {
        slotWindowDestroy(res);
        return NULL;
    }

    res->backTexture = createTexture(res->window->renderer, BACK_BUTTON_PATH);
    if (res->backTexture == NULL) {
        slotWindowDestroy(res);
        return NULL;
    }
    return res;

}

bool createSlotButtons(SPGameSlotsWin *win) {
    for (int i = 0; i < 5; i++) {
        char path[100];
        sprintf(path, SLOT_BUTTON_PATH_FORMAT, i + 1);
        win->slotsTextures[i] = createTexture(win->window->renderer, path);
        if (win->slotsTextures[i] == NULL) {
            return false;
        }

        sprintf(path, SLOT_BUTTON_INACTIVE_PATH_FORMAT, i + 1);
        win->slotsInactiveTextures[i] = createTexture(win->window->renderer, path);
        if (win->slotsInactiveTextures[i] == NULL) {
            return false;
        }
    }
    return true;
}

void slotWindowDraw(SPGameSlotsWin *src) {
    if (src == NULL) {
        return;
    }
    SDL_SetRenderDrawColor(src->window->renderer, 238, 238, 210, 255);
    SDL_RenderClear(src->window->renderer);
    for (int i = 0; i < src->slotsManager->numOfSlots; i++) {
        if (src->chosenSlot == i + 1) {
            SDL_RenderCopy(src->window->renderer, src->slotsTextures[i], NULL, &(slotsRects[i]));
        } else {
            SDL_RenderCopy(src->window->renderer, src->slotsInactiveTextures[i], NULL, &(slotsRects[i]));
        }
    }
    SDL_RenderCopy(src->window->renderer, src->backTexture, NULL, &backR_2);
    if (src->chosenSlot == 0) {
        SDL_RenderCopy(src->window->renderer, src->loadInactiveTexture, NULL, &loadR_2);
    } else {
        SDL_RenderCopy(src->window->renderer, src->loadTexture, NULL, &loadR_2);
    }
    SDL_RenderPresent(src->window->renderer);
}

void slotWindowDestroy(SPGameSlotsWin *src) {
    if (!src) {
        return;
    }
    if (src->backTexture != NULL) {
        SDL_DestroyTexture(src->backTexture);
    }
    if (src->loadInactiveTexture != NULL) {
        SDL_DestroyTexture(src->loadInactiveTexture);
    }
    if (src->loadTexture != NULL) {
        SDL_DestroyTexture(src->loadTexture);
    }
    for (int i = 0; i < 5; i++) {
        if (src->slotsTextures[i] != NULL) {
            SDL_DestroyTexture(src->slotsTextures[i]);
        }
        if (src->slotsInactiveTextures[i] != NULL) {
            SDL_DestroyTexture(src->slotsInactiveTextures[i]);
        }
    }

    destroyWindow(src->window);
    free(src);
}

void slotWindowHide(SPGameSlotsWin *src) {
    SDL_HideWindow(src->window->sdl_window);
}

void slotWindowShow(SPGameSlotsWin *src) {
    SDL_ShowWindow(src->window->sdl_window);
}

bool isClickOnSlot(int slotNumber, int x, int y) {
    return isInRectangle(x, y, slotsRects[slotNumber]);
}

bool spGameSlotsIsClickOnBack(int x, int y) {
    return isInRectangle(x, y, backR_2);

}

bool spGameSlotsIsClickOnLoad(int x, int y, SPGameSlotsWin* window) {
    return window->chosenSlot != 0 && isInRectangle(x, y, loadR_2);
}

SP_EVENT slotWindowHandleEvent(SPGameSlotsWin *src, SDL_Event *event) {
    if (!event) {
        return INVALID_EVENT;
    }
    switch (event->type) {
        case SDL_MOUSEBUTTONUP:
            for (int i = 0; i < src->slotsManager->numOfSlots; i++) {
                if (isClickOnSlot(i, event->button.x, event->button.y)) {
                    src->chosenSlot = i + 1;
                    return EVENT_CHOSE_SLOT;
                }
            }
            if (spGameSlotsIsClickOnBack(event->button.x, event->button.y)) {
                return EVENT_BACK;
            } else if (spGameSlotsIsClickOnLoad(event->button.x, event->button.y, src)) {
                return EVENT_LOAD_GAME;
            }
            break;

        case SDL_WINDOWEVENT:
            if (event->window.event == SDL_WINDOWEVENT_CLOSE) {
                return EVENT_QUIT;
            }
            break;

        default:
            return EVENT_NONE;
    }
    return EVENT_NONE;
}

