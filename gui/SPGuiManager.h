//
// created by Ofir & Shawn on 11/09/2017.
//

#ifndef CHESS_SPGUIMANAGER_H
#define CHESS_SPGUIMANAGER_H

#include "SPMainMenuWindow.h"
#include "SPSettingsMenu.h"
#include "../guiUtil/SPGuiUtil.h"
#include "SPGameSlotsWindow.h"
#include "../core/SPGameState.h"
#include "SPGameBoardWindow.h"

typedef enum{
    MAIN_MENU_ACTIVE,
    SETTINGS_WINDOW_ACTIVE,
    LOAD_MENU_ACTIVE,
    GAME_WINDOW_ACTIVE
} ACTIVE_WINDOW;


typedef struct {
    SPMainMenuWin* mainMenuWin;
    SPSettingsWin* settingsWin;
    SPGameSlotsWin* loadGameWin;
    SPGameBoardWin* gameBoardWin;
    SPGameSlotsManager* gameSlotsManager;
    ACTIVE_WINDOW activeWin;
    ACTIVE_WINDOW prevWin;
} SPGuiManager;

/**
 * creates a GUI manager instance, creates a main menu window, and sets the active window to the main menu
 * @return
 * GUI manager instance
 */
SPGuiManager* guiManagerCreate();

/**
 * Frees all memory resources associated with a gui manager instance.
 * if the source gui manager is NULL then the function does nothing
 * @param src - gui manager instance
 */
void guiManagerDestroy(SPGuiManager* src);

/**
 * draws the active window of a source gui manager
 * @param src - @NotNULL gui manager instance
 */
void guiManagerDraw(SPGuiManager* src);

/**
 * sends a mouse/window event to the event handler of the active window of a GUI manager.
 * returns the result of the event handling
 * @param src - @NotNULL gui manager instance
 * @param event - a window or mouse event
 * @return
 * result SP_EVENT of the event handling in the active window
 */
SP_EVENT guiManagerHandleEvent(SPGuiManager* src, SDL_Event* event) ;

/**
 * changes the active window of a GUI manager according to result of an event handling in the main menu window.
 * if event was pressing the New Game button,
 * then the function will hide the main menu and will set the active window to be the settings window.
 * if event was pressing the Load button,
 * then the function will hide the main menu and will set the active window to be the slots window,
 * and will set the previously active window to be the main menu.
 * @param src - @NotNULL gui manager instance
 * @param event - the event that occurred in the main menu window
 */
void handleManagerDueToMainMenuEvent(SPGuiManager *src, SP_EVENT *event);

/**
 * changes the active window of a GUI manager according to result of an event handling in the settings window.
 * if event was pressing the Back button,
 * then the function will hide the settings window and will set the active window to be the main menu.
 * if event was pressing the Start button,
 * then the function will destroy the settings window and will create a game window with the user's chosen settings
 * and will set it as the active window
 * @param src - @NotNULL gui manager instance
 * @param event - the event that occurred in the main menu window
 */
void handleManagerDueToSettingsEvent(SPGuiManager *src, SP_EVENT *event);

/**
 * changes the active window of a GUI manager according to result of an event handling in the slots window.
 * if event was pressing the Back button,
 * then the function will destroy the slots window and will set the active window to be the previously active window
 * if event was pressing the Load button,
 * then the function will destroy the slots window and will create a game window with the user's chosen slot file
 * and will set it as the active window
 * @param src - @NotNULL gui manager instance
 * @param event - the event that occurred in the main menu window
 */
void handleManagerDueToLoadEvent(SPGuiManager *src, SP_EVENT *event);

/**
 * changes the active window of a GUI manager according to result of an event handling in the game window.
 * if event was pressing the Main Menu button,
 * then the function will destroy the game window and will set the active window to be the main menu
 * if event was pressing the Load button,
 * then the function will hide the game window and will set the active window to be the slots window,
 * and will set the previously active window to be the game window
 * @param src - @NotNULL gui manager instance
 * @param event - the event that occurred in the main menu window
 */
void handleManagerDueToGameBoardEvent(SPGuiManager *src, SP_EVENT *event);

#endif //CHESS_SPGUIMANAGER_H
