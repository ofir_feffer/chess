/*
 *
 *  Created on: Sep 11, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#ifndef CHESS_GUIUTIL_H
#define CHESS_GUIUTIL_H

#include <stdbool.h>
#include <SDL.h>
#include <stdio.h>

typedef enum SP_EVENT {
    EVENT_MAIN_MENU,
    EVENT_NEW_GAME,
    EVENT_LOAD_MENU,
    EVENT_START_GAME,
    EVENT_LOAD_GAME,
    EVENT_SAVE_GAME,
    EVENT_QUIT,
    EVENT_CHANGED_SETTINGS,
    EVENT_CHOSE_SLOT,
    EVENT_DRAG_AND_DROP,
    EVENT_RESTART,
    EVENT_UNDO,
    EVENT_BACK,
    INVALID_EVENT,
    EVENT_NONE
} SP_EVENT;

#define LOAD_BMP_ERROR "couldn't create %s surface\n"

/**
 * creates a texture from a given path to a BMP file
 * @param renderer - @NotNULL an SDL_Renderer instance
 * @param path - @NotNULL a path to a BMP file
 * @return
 * NULL if an allocation error occurred or an I/O operation failed,
 * an instance of SDL_Texture otherwise
 */
SDL_Texture* createTexture(SDL_Renderer *renderer, const char* path);

/**
 * checks if a given point is inside a given rectangle
 * @param x - x-axis value of a point
 * @param y - y-axis value of a point
 * @param rectangle - a rectangle instance
 * @return
 * true if the point (x,y) is inside the given rectangle,
 * false otherwise
 */
bool isInRectangle(int x, int y, SDL_Rect rectangle);

#endif
