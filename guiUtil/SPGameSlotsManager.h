//
// created by Ofir & Shawnf on 13/09/2017.
//

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "../util/SPMessages.h"
#include <time.h>
#include "../core/SPGameState.h"

#ifndef CHESS_SPGAMESLOTS_H
#define CHESS_SPGAMESLOTS_H

// to fix issue that in windows mkdir should get only 1 parameter and in linux it should get 2 parameters
#if (defined(_WIN32) || defined(__WIN32__))
    #define mkdir(A, B) mkdir(A)
#endif

#define FILE_PATH_MAX_SIZE 50
#define MAX_SLOTS 5
#define MANAGEMENT_FILE "./saved_games/manager.txt"
#define SAVED_GAMES_DIRECTORY "./saved_games"
#define SAVE_FILE_PATH_FORMAT "./saved_games/%ld.xml"

typedef struct dirent DirEntry;

// a type used to represents game slots paths
typedef struct gameSlots {
    char* filePaths[MAX_SLOTS];
    int numOfSlots;
} SPGameSlotsManager;

/**
 * returns an instance of the game slots manager:
 *      filePaths will be initialized with the paths of the most recent saved games
 *      numOfSlots will be set to the total number of game slots that are occupied
 * @return
 *  NULL if an allocation error occurs, an instance of game slots manager otherwise
 */
SPGameSlotsManager* getGameSlotsManagerInstance();

/**
 * frees all memory resources associated with a given slots manager
 * @param manager - an instance of a slots manager
 */
void destroyGameSlotsManager(SPGameSlotsManager* manager);

/**
 * gets an instance of a game slots manager and an instance of a game state
 * saves the game and stores the saved game's path as the first slot
 * @param manager - game slots manager instance
 * @param gameState - game state instance
 * @return
 * true if parameters are not NULL and all I/O operations succeeded, false otherwise
 */
bool saveGameToFirstSlot(SPGameSlotsManager* manager, SPGameState* gameState);

/**
 * gets an instance of a slots manager and saves to the file 'manager.txt' all paths of current slots
 * @param manager - an instance of a slots manager
 * @return
 * true if all I/O operations succeeded, false otherwise
 */
bool writeSlotsDataToDisk(SPGameSlotsManager* manager);

/**
 * gets an instance of a slots manager and a slot's index.
 * loads the game in the given slot and returns a SPGameState instance.
 * @param manager - an instance of a slots manager
 * @param slot - a slot index
 * @return
 * NULL if slot index is out of bounds or if an I/O operation failed,
 * a SPGameState instance otherwise
 */
SPGameState* loadGameFromSlot(SPGameSlotsManager* manager, int slot);

/**
 * gets a string and removes a '\n' character from its end (if exists)
 * @param str - source string
 */
void removeTrailingNewLine(char* str);

#endif //CHESS_SPGAMESLOTS_H
