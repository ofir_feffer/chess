//
// created by Ofir & Shawnf on 13/09/2017.
//

#include "SPGameSlotsManager.h"

SPGameSlotsManager* getGameSlotsManagerInstance() {
    static SPGameSlotsManager* manager = NULL; // a singleton

    if (manager != NULL) {
        return manager;
    }

    manager = malloc(sizeof(SPGameSlotsManager));
    if (manager == NULL) {
        printf(MSG_MALLOC_ERR);
        return NULL;
    }
    manager->numOfSlots = 0;

    FILE* file = fopen(MANAGEMENT_FILE, "r");
    if (file == NULL) {
        return manager;
    }

    while (true) {
        char* line = malloc(FILE_PATH_MAX_SIZE);
        if (line == NULL) {
            printf(MSG_MALLOC_ERR);
            break;
        }
        if (fgets(line, FILE_PATH_MAX_SIZE, file) == NULL) { // read next line of file, each line is a path to a saved game
            free(line);
            break;
        }
        removeTrailingNewLine(line); //remove the '\n' at the end of the string
        manager->filePaths[manager->numOfSlots] = line; // store the slot's path
        manager->numOfSlots++;
    }
    fclose(file);
    return manager;
}

void destroyGameSlotsManager(SPGameSlotsManager* manager) {
    if (manager == NULL) {
        return;
    }
    for (int i = 0; i < manager->numOfSlots; i++) {
        free(manager->filePaths[i]);
    }
    free(manager);
}

bool saveGameToFirstSlot(SPGameSlotsManager* manager, SPGameState* gameState) {
    if (manager == NULL || gameState == NULL) {
        return false;
    }

    mkdir(SAVED_GAMES_DIRECTORY, S_IRUSR | S_IWUSR | S_IXUSR); // make folder in case it doesn't exist
    long currentTime = (long) time(0);

    char* path = malloc(FILE_PATH_MAX_SIZE);
    if (path == NULL) {
        printf(MSG_MALLOC_ERR);
        return false;
    }
    sprintf(path, SAVE_FILE_PATH_FORMAT, currentTime); // set path to ./saved_games/{currentTime}.xml

    bool res = saveGameStateToFile(gameState, path); // save game
    if (!res) {
        return false;
    }

    if (manager->numOfSlots == MAX_SLOTS) { // if all slots are occupied, free the last one
        free(manager->filePaths[MAX_SLOTS - 1]);
        manager->numOfSlots--;
    }
    for (int i = manager->numOfSlots; i > 0; i--) { // shift all slots to the right
        manager->filePaths[i] = manager->filePaths[i-1];
    }
    manager->filePaths[0] = path; // store the new path as the first slot
    manager->numOfSlots++;

    return writeSlotsDataToDisk(manager);
}

bool writeSlotsDataToDisk(SPGameSlotsManager* manager) {
    FILE* file = fopen(MANAGEMENT_FILE, "w");
    if (file == NULL) {
        return false;
    }

    for (int i = 0; i < manager->numOfSlots; i++) {
        fprintf(file, "%s\n", manager->filePaths[i]);
    }
    fclose(file);
    return true;
}

SPGameState* loadGameFromSlot(SPGameSlotsManager* manager, int slot) {
    if (slot < 0 || slot >= manager->numOfSlots ) {
        return NULL;
    }
    SPGameState* res = createGameStateFromFile(manager->filePaths[slot]);
    return res;
}

void removeTrailingNewLine(char* str) {
    if (str == NULL) {
        return;
    }

    int length = strlen(str);
    if (length - 1 >= 0 && str[length - 1] == '\n') {
        str[length - 1] = '\0';
    }
}