/*
 *
 *  Created on: Sep 11, 2017
 *      Author: Shawn Molga & Ofir Feffer
 */

#include "SPGuiUtil.h"

SDL_Texture *createTexture(SDL_Renderer *renderer, const char *path) {
    SDL_Surface *loadingSurface = SDL_LoadBMP(path);
    if (loadingSurface == NULL) {
        printf(LOAD_BMP_ERROR, path);
        return NULL;
    }
    SDL_SetColorKey(loadingSurface, SDL_TRUE, SDL_MapRGB(loadingSurface->format, 0, 0, 0));
    SDL_Texture *res = SDL_CreateTextureFromSurface(renderer, loadingSurface);
    if (res == NULL) {
        SDL_FreeSurface(loadingSurface);
        printf(LOAD_BMP_ERROR, path);
        return NULL;
    }
    SDL_FreeSurface(loadingSurface);
    return res;
}

bool isInRectangle(int x, int y, SDL_Rect rectangle) {
    if (x >= rectangle.x && x <= rectangle.x + rectangle.w && y >= rectangle.y && y <= rectangle.y + rectangle.h) {
        return true;
    }
    return false;
}


